/*
 * generated by Xtext
 */
package org.xtext.example.mydsl.services;

import com.google.inject.Singleton;
import com.google.inject.Inject;

import java.util.List;

import org.eclipse.xtext.*;
import org.eclipse.xtext.service.GrammarProvider;
import org.eclipse.xtext.service.AbstractElementFinder.*;

import org.eclipse.xtext.common.services.TerminalsGrammarAccess;

@Singleton
public class PetrinetDslGrammarAccess extends AbstractGrammarElementFinder {
	
	
	public class PetriNetElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "PetriNet");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Action cPetriNetAction_0 = (Action)cGroup.eContents().get(0);
		private final RuleCall cLESSTerminalRuleCall_1 = (RuleCall)cGroup.eContents().get(1);
		private final RuleCall cQUESTIONTerminalRuleCall_2 = (RuleCall)cGroup.eContents().get(2);
		private final RuleCall cXMLTerminalRuleCall_3 = (RuleCall)cGroup.eContents().get(3);
		private final RuleCall cVERSIONTerminalRuleCall_4 = (RuleCall)cGroup.eContents().get(4);
		private final RuleCall cEQUALSTerminalRuleCall_5 = (RuleCall)cGroup.eContents().get(5);
		private final RuleCall cSTRINGTerminalRuleCall_6 = (RuleCall)cGroup.eContents().get(6);
		private final RuleCall cQUESTIONTerminalRuleCall_7 = (RuleCall)cGroup.eContents().get(7);
		private final RuleCall cGREATERTerminalRuleCall_8 = (RuleCall)cGroup.eContents().get(8);
		private final RuleCall cLESSTerminalRuleCall_9 = (RuleCall)cGroup.eContents().get(9);
		private final RuleCall cPNMLTerminalRuleCall_10 = (RuleCall)cGroup.eContents().get(10);
		private final RuleCall cCOLONTerminalRuleCall_11 = (RuleCall)cGroup.eContents().get(11);
		private final RuleCall cPNMLTerminalRuleCall_12 = (RuleCall)cGroup.eContents().get(12);
		private final RuleCall cXMLNSTerminalRuleCall_13 = (RuleCall)cGroup.eContents().get(13);
		private final RuleCall cCOLONTerminalRuleCall_14 = (RuleCall)cGroup.eContents().get(14);
		private final RuleCall cPNMLTerminalRuleCall_15 = (RuleCall)cGroup.eContents().get(15);
		private final RuleCall cEQUALSTerminalRuleCall_16 = (RuleCall)cGroup.eContents().get(16);
		private final RuleCall cSTRINGTerminalRuleCall_17 = (RuleCall)cGroup.eContents().get(17);
		private final RuleCall cGREATERTerminalRuleCall_18 = (RuleCall)cGroup.eContents().get(18);
		private final RuleCall cLESSTerminalRuleCall_19 = (RuleCall)cGroup.eContents().get(19);
		private final RuleCall cNETTerminalRuleCall_20 = (RuleCall)cGroup.eContents().get(20);
		private final RuleCall cTYPETerminalRuleCall_21 = (RuleCall)cGroup.eContents().get(21);
		private final RuleCall cEQUALSTerminalRuleCall_22 = (RuleCall)cGroup.eContents().get(22);
		private final RuleCall cSTRINGTerminalRuleCall_23 = (RuleCall)cGroup.eContents().get(23);
		private final RuleCall cGREATERTerminalRuleCall_24 = (RuleCall)cGroup.eContents().get(24);
		private final Assignment cElementsAssignment_25 = (Assignment)cGroup.eContents().get(25);
		private final RuleCall cElementsElementParserRuleCall_25_0 = (RuleCall)cElementsAssignment_25.eContents().get(0);
		private final RuleCall cLESSTerminalRuleCall_26 = (RuleCall)cGroup.eContents().get(26);
		private final RuleCall cSLASHTerminalRuleCall_27 = (RuleCall)cGroup.eContents().get(27);
		private final RuleCall cNETTerminalRuleCall_28 = (RuleCall)cGroup.eContents().get(28);
		private final RuleCall cGREATERTerminalRuleCall_29 = (RuleCall)cGroup.eContents().get(29);
		private final RuleCall cLESSTerminalRuleCall_30 = (RuleCall)cGroup.eContents().get(30);
		private final RuleCall cSLASHTerminalRuleCall_31 = (RuleCall)cGroup.eContents().get(31);
		private final RuleCall cPNMLTerminalRuleCall_32 = (RuleCall)cGroup.eContents().get(32);
		private final RuleCall cCOLONTerminalRuleCall_33 = (RuleCall)cGroup.eContents().get(33);
		private final RuleCall cPNMLTerminalRuleCall_34 = (RuleCall)cGroup.eContents().get(34);
		private final RuleCall cGREATERTerminalRuleCall_35 = (RuleCall)cGroup.eContents().get(35);
		
		//PetriNet:
		//	{PetriNet} LESS QUESTION XML VERSION EQUALS STRING QUESTION GREATER LESS PNML COLON PNML XMLNS COLON PNML EQUALS
		//	STRING GREATER LESS NET TYPE EQUALS STRING GREATER elements+=Element* LESS SLASH NET GREATER LESS SLASH PNML COLON
		//	PNML GREATER;
		@Override public ParserRule getRule() { return rule; }

		//{PetriNet} LESS QUESTION XML VERSION EQUALS STRING QUESTION GREATER LESS PNML COLON PNML XMLNS COLON PNML EQUALS STRING
		//GREATER LESS NET TYPE EQUALS STRING GREATER elements+=Element* LESS SLASH NET GREATER LESS SLASH PNML COLON PNML
		//GREATER
		public Group getGroup() { return cGroup; }

		//{PetriNet}
		public Action getPetriNetAction_0() { return cPetriNetAction_0; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_1() { return cLESSTerminalRuleCall_1; }

		//QUESTION
		public RuleCall getQUESTIONTerminalRuleCall_2() { return cQUESTIONTerminalRuleCall_2; }

		//XML
		public RuleCall getXMLTerminalRuleCall_3() { return cXMLTerminalRuleCall_3; }

		//VERSION
		public RuleCall getVERSIONTerminalRuleCall_4() { return cVERSIONTerminalRuleCall_4; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_5() { return cEQUALSTerminalRuleCall_5; }

		//STRING
		public RuleCall getSTRINGTerminalRuleCall_6() { return cSTRINGTerminalRuleCall_6; }

		//QUESTION
		public RuleCall getQUESTIONTerminalRuleCall_7() { return cQUESTIONTerminalRuleCall_7; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_8() { return cGREATERTerminalRuleCall_8; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_9() { return cLESSTerminalRuleCall_9; }

		//PNML
		public RuleCall getPNMLTerminalRuleCall_10() { return cPNMLTerminalRuleCall_10; }

		//COLON
		public RuleCall getCOLONTerminalRuleCall_11() { return cCOLONTerminalRuleCall_11; }

		//PNML
		public RuleCall getPNMLTerminalRuleCall_12() { return cPNMLTerminalRuleCall_12; }

		//XMLNS
		public RuleCall getXMLNSTerminalRuleCall_13() { return cXMLNSTerminalRuleCall_13; }

		//COLON
		public RuleCall getCOLONTerminalRuleCall_14() { return cCOLONTerminalRuleCall_14; }

		//PNML
		public RuleCall getPNMLTerminalRuleCall_15() { return cPNMLTerminalRuleCall_15; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_16() { return cEQUALSTerminalRuleCall_16; }

		//STRING
		public RuleCall getSTRINGTerminalRuleCall_17() { return cSTRINGTerminalRuleCall_17; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_18() { return cGREATERTerminalRuleCall_18; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_19() { return cLESSTerminalRuleCall_19; }

		//NET
		public RuleCall getNETTerminalRuleCall_20() { return cNETTerminalRuleCall_20; }

		//TYPE
		public RuleCall getTYPETerminalRuleCall_21() { return cTYPETerminalRuleCall_21; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_22() { return cEQUALSTerminalRuleCall_22; }

		//STRING
		public RuleCall getSTRINGTerminalRuleCall_23() { return cSTRINGTerminalRuleCall_23; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_24() { return cGREATERTerminalRuleCall_24; }

		//elements+=Element*
		public Assignment getElementsAssignment_25() { return cElementsAssignment_25; }

		//Element
		public RuleCall getElementsElementParserRuleCall_25_0() { return cElementsElementParserRuleCall_25_0; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_26() { return cLESSTerminalRuleCall_26; }

		//SLASH
		public RuleCall getSLASHTerminalRuleCall_27() { return cSLASHTerminalRuleCall_27; }

		//NET
		public RuleCall getNETTerminalRuleCall_28() { return cNETTerminalRuleCall_28; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_29() { return cGREATERTerminalRuleCall_29; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_30() { return cLESSTerminalRuleCall_30; }

		//SLASH
		public RuleCall getSLASHTerminalRuleCall_31() { return cSLASHTerminalRuleCall_31; }

		//PNML
		public RuleCall getPNMLTerminalRuleCall_32() { return cPNMLTerminalRuleCall_32; }

		//COLON
		public RuleCall getCOLONTerminalRuleCall_33() { return cCOLONTerminalRuleCall_33; }

		//PNML
		public RuleCall getPNMLTerminalRuleCall_34() { return cPNMLTerminalRuleCall_34; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_35() { return cGREATERTerminalRuleCall_35; }
	}

	public class ElementElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "Element");
		private final Alternatives cAlternatives = (Alternatives)rule.eContents().get(1);
		private final RuleCall cOutputArcParserRuleCall_0 = (RuleCall)cAlternatives.eContents().get(0);
		private final RuleCall cInputArcParserRuleCall_1 = (RuleCall)cAlternatives.eContents().get(1);
		private final RuleCall cTransitionParserRuleCall_2 = (RuleCall)cAlternatives.eContents().get(2);
		private final RuleCall cPlaceParserRuleCall_3 = (RuleCall)cAlternatives.eContents().get(3);
		
		//Element:
		//	OutputArc | InputArc | Transition | Place;
		@Override public ParserRule getRule() { return rule; }

		//OutputArc | InputArc | Transition | Place
		public Alternatives getAlternatives() { return cAlternatives; }

		//OutputArc
		public RuleCall getOutputArcParserRuleCall_0() { return cOutputArcParserRuleCall_0; }

		//InputArc
		public RuleCall getInputArcParserRuleCall_1() { return cInputArcParserRuleCall_1; }

		//Transition
		public RuleCall getTransitionParserRuleCall_2() { return cTransitionParserRuleCall_2; }

		//Place
		public RuleCall getPlaceParserRuleCall_3() { return cPlaceParserRuleCall_3; }
	}

	public class InputArcElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "InputArc");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cLESSTerminalRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final RuleCall cTINPUTARCTerminalRuleCall_1 = (RuleCall)cGroup.eContents().get(1);
		private final UnorderedGroup cUnorderedGroup_2 = (UnorderedGroup)cGroup.eContents().get(2);
		private final Group cGroup_2_0 = (Group)cUnorderedGroup_2.eContents().get(0);
		private final RuleCall cIDTerminalRuleCall_2_0_0 = (RuleCall)cGroup_2_0.eContents().get(0);
		private final RuleCall cEQUALSTerminalRuleCall_2_0_1 = (RuleCall)cGroup_2_0.eContents().get(1);
		private final Assignment cNameAssignment_2_0_2 = (Assignment)cGroup_2_0.eContents().get(2);
		private final RuleCall cNameSTRINGTerminalRuleCall_2_0_2_0 = (RuleCall)cNameAssignment_2_0_2.eContents().get(0);
		private final Group cGroup_2_1 = (Group)cUnorderedGroup_2.eContents().get(1);
		private final RuleCall cSOURCETerminalRuleCall_2_1_0 = (RuleCall)cGroup_2_1.eContents().get(0);
		private final RuleCall cEQUALSTerminalRuleCall_2_1_1 = (RuleCall)cGroup_2_1.eContents().get(1);
		private final Assignment cFromAssignment_2_1_2 = (Assignment)cGroup_2_1.eContents().get(2);
		private final CrossReference cFromPlaceCrossReference_2_1_2_0 = (CrossReference)cFromAssignment_2_1_2.eContents().get(0);
		private final RuleCall cFromPlaceSTRINGTerminalRuleCall_2_1_2_0_1 = (RuleCall)cFromPlaceCrossReference_2_1_2_0.eContents().get(1);
		private final Group cGroup_2_2 = (Group)cUnorderedGroup_2.eContents().get(2);
		private final RuleCall cTARGETTerminalRuleCall_2_2_0 = (RuleCall)cGroup_2_2.eContents().get(0);
		private final RuleCall cEQUALSTerminalRuleCall_2_2_1 = (RuleCall)cGroup_2_2.eContents().get(1);
		private final Assignment cToAssignment_2_2_2 = (Assignment)cGroup_2_2.eContents().get(2);
		private final CrossReference cToTransitionCrossReference_2_2_2_0 = (CrossReference)cToAssignment_2_2_2.eContents().get(0);
		private final RuleCall cToTransitionSTRINGTerminalRuleCall_2_2_2_0_1 = (RuleCall)cToTransitionCrossReference_2_2_2_0.eContents().get(1);
		private final RuleCall cGREATERTerminalRuleCall_3 = (RuleCall)cGroup.eContents().get(3);
		private final RuleCall cLESSTerminalRuleCall_4 = (RuleCall)cGroup.eContents().get(4);
		private final RuleCall cSLASHTerminalRuleCall_5 = (RuleCall)cGroup.eContents().get(5);
		private final RuleCall cTINPUTARCTerminalRuleCall_6 = (RuleCall)cGroup.eContents().get(6);
		private final RuleCall cGREATERTerminalRuleCall_7 = (RuleCall)cGroup.eContents().get(7);
		
		//InputArc:
		//	LESS TINPUTARC (ID EQUALS name=STRING & SOURCE EQUALS from=[Place|STRING] & TARGET EQUALS to=[Transition|STRING])
		//	GREATER LESS SLASH TINPUTARC GREATER;
		@Override public ParserRule getRule() { return rule; }

		//LESS TINPUTARC (ID EQUALS name=STRING & SOURCE EQUALS from=[Place|STRING] & TARGET EQUALS to=[Transition|STRING])
		//GREATER LESS SLASH TINPUTARC GREATER
		public Group getGroup() { return cGroup; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_0() { return cLESSTerminalRuleCall_0; }

		//TINPUTARC
		public RuleCall getTINPUTARCTerminalRuleCall_1() { return cTINPUTARCTerminalRuleCall_1; }

		//ID EQUALS name=STRING & SOURCE EQUALS from=[Place|STRING] & TARGET EQUALS to=[Transition|STRING]
		public UnorderedGroup getUnorderedGroup_2() { return cUnorderedGroup_2; }

		//ID EQUALS name=STRING
		public Group getGroup_2_0() { return cGroup_2_0; }

		//ID
		public RuleCall getIDTerminalRuleCall_2_0_0() { return cIDTerminalRuleCall_2_0_0; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_2_0_1() { return cEQUALSTerminalRuleCall_2_0_1; }

		//name=STRING
		public Assignment getNameAssignment_2_0_2() { return cNameAssignment_2_0_2; }

		//STRING
		public RuleCall getNameSTRINGTerminalRuleCall_2_0_2_0() { return cNameSTRINGTerminalRuleCall_2_0_2_0; }

		//SOURCE EQUALS from=[Place|STRING]
		public Group getGroup_2_1() { return cGroup_2_1; }

		//SOURCE
		public RuleCall getSOURCETerminalRuleCall_2_1_0() { return cSOURCETerminalRuleCall_2_1_0; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_2_1_1() { return cEQUALSTerminalRuleCall_2_1_1; }

		//from=[Place|STRING]
		public Assignment getFromAssignment_2_1_2() { return cFromAssignment_2_1_2; }

		//[Place|STRING]
		public CrossReference getFromPlaceCrossReference_2_1_2_0() { return cFromPlaceCrossReference_2_1_2_0; }

		//STRING
		public RuleCall getFromPlaceSTRINGTerminalRuleCall_2_1_2_0_1() { return cFromPlaceSTRINGTerminalRuleCall_2_1_2_0_1; }

		//TARGET EQUALS to=[Transition|STRING]
		public Group getGroup_2_2() { return cGroup_2_2; }

		//TARGET
		public RuleCall getTARGETTerminalRuleCall_2_2_0() { return cTARGETTerminalRuleCall_2_2_0; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_2_2_1() { return cEQUALSTerminalRuleCall_2_2_1; }

		//to=[Transition|STRING]
		public Assignment getToAssignment_2_2_2() { return cToAssignment_2_2_2; }

		//[Transition|STRING]
		public CrossReference getToTransitionCrossReference_2_2_2_0() { return cToTransitionCrossReference_2_2_2_0; }

		//STRING
		public RuleCall getToTransitionSTRINGTerminalRuleCall_2_2_2_0_1() { return cToTransitionSTRINGTerminalRuleCall_2_2_2_0_1; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_3() { return cGREATERTerminalRuleCall_3; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_4() { return cLESSTerminalRuleCall_4; }

		//SLASH
		public RuleCall getSLASHTerminalRuleCall_5() { return cSLASHTerminalRuleCall_5; }

		//TINPUTARC
		public RuleCall getTINPUTARCTerminalRuleCall_6() { return cTINPUTARCTerminalRuleCall_6; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_7() { return cGREATERTerminalRuleCall_7; }
	}

	public class PlaceElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "Place");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cLESSTerminalRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final RuleCall cTPLACETerminalRuleCall_1 = (RuleCall)cGroup.eContents().get(1);
		private final RuleCall cIDTerminalRuleCall_2 = (RuleCall)cGroup.eContents().get(2);
		private final RuleCall cEQUALSTerminalRuleCall_3 = (RuleCall)cGroup.eContents().get(3);
		private final Assignment cNameAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cNameSTRINGTerminalRuleCall_4_0 = (RuleCall)cNameAssignment_4.eContents().get(0);
		private final RuleCall cGREATERTerminalRuleCall_5 = (RuleCall)cGroup.eContents().get(5);
		private final RuleCall cLESSTerminalRuleCall_6 = (RuleCall)cGroup.eContents().get(6);
		private final RuleCall cSLASHTerminalRuleCall_7 = (RuleCall)cGroup.eContents().get(7);
		private final RuleCall cTPLACETerminalRuleCall_8 = (RuleCall)cGroup.eContents().get(8);
		private final RuleCall cGREATERTerminalRuleCall_9 = (RuleCall)cGroup.eContents().get(9);
		
		//Place:
		//	LESS TPLACE ID EQUALS name=STRING GREATER LESS SLASH TPLACE GREATER;
		@Override public ParserRule getRule() { return rule; }

		//LESS TPLACE ID EQUALS name=STRING GREATER LESS SLASH TPLACE GREATER
		public Group getGroup() { return cGroup; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_0() { return cLESSTerminalRuleCall_0; }

		//TPLACE
		public RuleCall getTPLACETerminalRuleCall_1() { return cTPLACETerminalRuleCall_1; }

		//ID
		public RuleCall getIDTerminalRuleCall_2() { return cIDTerminalRuleCall_2; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_3() { return cEQUALSTerminalRuleCall_3; }

		//name=STRING
		public Assignment getNameAssignment_4() { return cNameAssignment_4; }

		//STRING
		public RuleCall getNameSTRINGTerminalRuleCall_4_0() { return cNameSTRINGTerminalRuleCall_4_0; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_5() { return cGREATERTerminalRuleCall_5; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_6() { return cLESSTerminalRuleCall_6; }

		//SLASH
		public RuleCall getSLASHTerminalRuleCall_7() { return cSLASHTerminalRuleCall_7; }

		//TPLACE
		public RuleCall getTPLACETerminalRuleCall_8() { return cTPLACETerminalRuleCall_8; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_9() { return cGREATERTerminalRuleCall_9; }
	}

	public class TransitionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "Transition");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cLESSTerminalRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final RuleCall cTTRANSITIONTerminalRuleCall_1 = (RuleCall)cGroup.eContents().get(1);
		private final RuleCall cIDTerminalRuleCall_2 = (RuleCall)cGroup.eContents().get(2);
		private final RuleCall cEQUALSTerminalRuleCall_3 = (RuleCall)cGroup.eContents().get(3);
		private final Assignment cNameAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cNameSTRINGTerminalRuleCall_4_0 = (RuleCall)cNameAssignment_4.eContents().get(0);
		private final Group cGroup_5 = (Group)cGroup.eContents().get(5);
		private final RuleCall cMAXDELAYTerminalRuleCall_5_0 = (RuleCall)cGroup_5.eContents().get(0);
		private final RuleCall cEQUALSTerminalRuleCall_5_1 = (RuleCall)cGroup_5.eContents().get(1);
		private final Assignment cMaxDelayAssignment_5_2 = (Assignment)cGroup_5.eContents().get(2);
		private final RuleCall cMaxDelayDOUBLEParserRuleCall_5_2_0 = (RuleCall)cMaxDelayAssignment_5_2.eContents().get(0);
		private final Group cGroup_6 = (Group)cGroup.eContents().get(6);
		private final RuleCall cMINDELAYTerminalRuleCall_6_0 = (RuleCall)cGroup_6.eContents().get(0);
		private final RuleCall cEQUALSTerminalRuleCall_6_1 = (RuleCall)cGroup_6.eContents().get(1);
		private final Assignment cMinDelayAssignment_6_2 = (Assignment)cGroup_6.eContents().get(2);
		private final RuleCall cMinDelayDOUBLEParserRuleCall_6_2_0 = (RuleCall)cMinDelayAssignment_6_2.eContents().get(0);
		private final RuleCall cGREATERTerminalRuleCall_7 = (RuleCall)cGroup.eContents().get(7);
		private final RuleCall cLESSTerminalRuleCall_8 = (RuleCall)cGroup.eContents().get(8);
		private final RuleCall cSLASHTerminalRuleCall_9 = (RuleCall)cGroup.eContents().get(9);
		private final RuleCall cTTRANSITIONTerminalRuleCall_10 = (RuleCall)cGroup.eContents().get(10);
		private final RuleCall cGREATERTerminalRuleCall_11 = (RuleCall)cGroup.eContents().get(11);
		
		//Transition:
		//	LESS TTRANSITION ID EQUALS name=STRING (MAXDELAY EQUALS maxDelay=DOUBLE)? (MINDELAY EQUALS minDelay=DOUBLE)? GREATER
		//	LESS SLASH TTRANSITION GREATER;
		@Override public ParserRule getRule() { return rule; }

		//LESS TTRANSITION ID EQUALS name=STRING (MAXDELAY EQUALS maxDelay=DOUBLE)? (MINDELAY EQUALS minDelay=DOUBLE)? GREATER
		//LESS SLASH TTRANSITION GREATER
		public Group getGroup() { return cGroup; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_0() { return cLESSTerminalRuleCall_0; }

		//TTRANSITION
		public RuleCall getTTRANSITIONTerminalRuleCall_1() { return cTTRANSITIONTerminalRuleCall_1; }

		//ID
		public RuleCall getIDTerminalRuleCall_2() { return cIDTerminalRuleCall_2; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_3() { return cEQUALSTerminalRuleCall_3; }

		//name=STRING
		public Assignment getNameAssignment_4() { return cNameAssignment_4; }

		//STRING
		public RuleCall getNameSTRINGTerminalRuleCall_4_0() { return cNameSTRINGTerminalRuleCall_4_0; }

		//(MAXDELAY EQUALS maxDelay=DOUBLE)?
		public Group getGroup_5() { return cGroup_5; }

		//MAXDELAY
		public RuleCall getMAXDELAYTerminalRuleCall_5_0() { return cMAXDELAYTerminalRuleCall_5_0; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_5_1() { return cEQUALSTerminalRuleCall_5_1; }

		//maxDelay=DOUBLE
		public Assignment getMaxDelayAssignment_5_2() { return cMaxDelayAssignment_5_2; }

		//DOUBLE
		public RuleCall getMaxDelayDOUBLEParserRuleCall_5_2_0() { return cMaxDelayDOUBLEParserRuleCall_5_2_0; }

		//(MINDELAY EQUALS minDelay=DOUBLE)?
		public Group getGroup_6() { return cGroup_6; }

		//MINDELAY
		public RuleCall getMINDELAYTerminalRuleCall_6_0() { return cMINDELAYTerminalRuleCall_6_0; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_6_1() { return cEQUALSTerminalRuleCall_6_1; }

		//minDelay=DOUBLE
		public Assignment getMinDelayAssignment_6_2() { return cMinDelayAssignment_6_2; }

		//DOUBLE
		public RuleCall getMinDelayDOUBLEParserRuleCall_6_2_0() { return cMinDelayDOUBLEParserRuleCall_6_2_0; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_7() { return cGREATERTerminalRuleCall_7; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_8() { return cLESSTerminalRuleCall_8; }

		//SLASH
		public RuleCall getSLASHTerminalRuleCall_9() { return cSLASHTerminalRuleCall_9; }

		//TTRANSITION
		public RuleCall getTTRANSITIONTerminalRuleCall_10() { return cTTRANSITIONTerminalRuleCall_10; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_11() { return cGREATERTerminalRuleCall_11; }
	}

	public class OutputArcElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "OutputArc");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cLESSTerminalRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final RuleCall cTOUTPUTARCTerminalRuleCall_1 = (RuleCall)cGroup.eContents().get(1);
		private final UnorderedGroup cUnorderedGroup_2 = (UnorderedGroup)cGroup.eContents().get(2);
		private final Group cGroup_2_0 = (Group)cUnorderedGroup_2.eContents().get(0);
		private final RuleCall cIDTerminalRuleCall_2_0_0 = (RuleCall)cGroup_2_0.eContents().get(0);
		private final RuleCall cEQUALSTerminalRuleCall_2_0_1 = (RuleCall)cGroup_2_0.eContents().get(1);
		private final Assignment cNameAssignment_2_0_2 = (Assignment)cGroup_2_0.eContents().get(2);
		private final RuleCall cNameSTRINGTerminalRuleCall_2_0_2_0 = (RuleCall)cNameAssignment_2_0_2.eContents().get(0);
		private final Group cGroup_2_1 = (Group)cUnorderedGroup_2.eContents().get(1);
		private final RuleCall cSOURCETerminalRuleCall_2_1_0 = (RuleCall)cGroup_2_1.eContents().get(0);
		private final RuleCall cEQUALSTerminalRuleCall_2_1_1 = (RuleCall)cGroup_2_1.eContents().get(1);
		private final Assignment cFromAssignment_2_1_2 = (Assignment)cGroup_2_1.eContents().get(2);
		private final CrossReference cFromTransitionCrossReference_2_1_2_0 = (CrossReference)cFromAssignment_2_1_2.eContents().get(0);
		private final RuleCall cFromTransitionSTRINGTerminalRuleCall_2_1_2_0_1 = (RuleCall)cFromTransitionCrossReference_2_1_2_0.eContents().get(1);
		private final Group cGroup_2_2 = (Group)cUnorderedGroup_2.eContents().get(2);
		private final RuleCall cTARGETTerminalRuleCall_2_2_0 = (RuleCall)cGroup_2_2.eContents().get(0);
		private final RuleCall cEQUALSTerminalRuleCall_2_2_1 = (RuleCall)cGroup_2_2.eContents().get(1);
		private final Assignment cToAssignment_2_2_2 = (Assignment)cGroup_2_2.eContents().get(2);
		private final CrossReference cToPlaceCrossReference_2_2_2_0 = (CrossReference)cToAssignment_2_2_2.eContents().get(0);
		private final RuleCall cToPlaceSTRINGTerminalRuleCall_2_2_2_0_1 = (RuleCall)cToPlaceCrossReference_2_2_2_0.eContents().get(1);
		private final RuleCall cGREATERTerminalRuleCall_3 = (RuleCall)cGroup.eContents().get(3);
		private final RuleCall cLESSTerminalRuleCall_4 = (RuleCall)cGroup.eContents().get(4);
		private final RuleCall cSLASHTerminalRuleCall_5 = (RuleCall)cGroup.eContents().get(5);
		private final RuleCall cTOUTPUTARCTerminalRuleCall_6 = (RuleCall)cGroup.eContents().get(6);
		private final RuleCall cGREATERTerminalRuleCall_7 = (RuleCall)cGroup.eContents().get(7);
		
		//OutputArc:
		//	LESS TOUTPUTARC (ID EQUALS name=STRING & SOURCE EQUALS from=[Transition|STRING] & TARGET EQUALS to=[Place|STRING])
		//	GREATER LESS SLASH TOUTPUTARC GREATER;
		@Override public ParserRule getRule() { return rule; }

		//LESS TOUTPUTARC (ID EQUALS name=STRING & SOURCE EQUALS from=[Transition|STRING] & TARGET EQUALS to=[Place|STRING])
		//GREATER LESS SLASH TOUTPUTARC GREATER
		public Group getGroup() { return cGroup; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_0() { return cLESSTerminalRuleCall_0; }

		//TOUTPUTARC
		public RuleCall getTOUTPUTARCTerminalRuleCall_1() { return cTOUTPUTARCTerminalRuleCall_1; }

		//ID EQUALS name=STRING & SOURCE EQUALS from=[Transition|STRING] & TARGET EQUALS to=[Place|STRING]
		public UnorderedGroup getUnorderedGroup_2() { return cUnorderedGroup_2; }

		//ID EQUALS name=STRING
		public Group getGroup_2_0() { return cGroup_2_0; }

		//ID
		public RuleCall getIDTerminalRuleCall_2_0_0() { return cIDTerminalRuleCall_2_0_0; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_2_0_1() { return cEQUALSTerminalRuleCall_2_0_1; }

		//name=STRING
		public Assignment getNameAssignment_2_0_2() { return cNameAssignment_2_0_2; }

		//STRING
		public RuleCall getNameSTRINGTerminalRuleCall_2_0_2_0() { return cNameSTRINGTerminalRuleCall_2_0_2_0; }

		//SOURCE EQUALS from=[Transition|STRING]
		public Group getGroup_2_1() { return cGroup_2_1; }

		//SOURCE
		public RuleCall getSOURCETerminalRuleCall_2_1_0() { return cSOURCETerminalRuleCall_2_1_0; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_2_1_1() { return cEQUALSTerminalRuleCall_2_1_1; }

		//from=[Transition|STRING]
		public Assignment getFromAssignment_2_1_2() { return cFromAssignment_2_1_2; }

		//[Transition|STRING]
		public CrossReference getFromTransitionCrossReference_2_1_2_0() { return cFromTransitionCrossReference_2_1_2_0; }

		//STRING
		public RuleCall getFromTransitionSTRINGTerminalRuleCall_2_1_2_0_1() { return cFromTransitionSTRINGTerminalRuleCall_2_1_2_0_1; }

		//TARGET EQUALS to=[Place|STRING]
		public Group getGroup_2_2() { return cGroup_2_2; }

		//TARGET
		public RuleCall getTARGETTerminalRuleCall_2_2_0() { return cTARGETTerminalRuleCall_2_2_0; }

		//EQUALS
		public RuleCall getEQUALSTerminalRuleCall_2_2_1() { return cEQUALSTerminalRuleCall_2_2_1; }

		//to=[Place|STRING]
		public Assignment getToAssignment_2_2_2() { return cToAssignment_2_2_2; }

		//[Place|STRING]
		public CrossReference getToPlaceCrossReference_2_2_2_0() { return cToPlaceCrossReference_2_2_2_0; }

		//STRING
		public RuleCall getToPlaceSTRINGTerminalRuleCall_2_2_2_0_1() { return cToPlaceSTRINGTerminalRuleCall_2_2_2_0_1; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_3() { return cGREATERTerminalRuleCall_3; }

		//LESS
		public RuleCall getLESSTerminalRuleCall_4() { return cLESSTerminalRuleCall_4; }

		//SLASH
		public RuleCall getSLASHTerminalRuleCall_5() { return cSLASHTerminalRuleCall_5; }

		//TOUTPUTARC
		public RuleCall getTOUTPUTARCTerminalRuleCall_6() { return cTOUTPUTARCTerminalRuleCall_6; }

		//GREATER
		public RuleCall getGREATERTerminalRuleCall_7() { return cGREATERTerminalRuleCall_7; }
	}

	public class DOUBLEElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "DOUBLE");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cINTTerminalRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Keyword cFullStopKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final RuleCall cINTTerminalRuleCall_2 = (RuleCall)cGroup.eContents().get(2);
		
		//DOUBLE returns ecore::EDouble:
		//	INT "." INT;
		@Override public ParserRule getRule() { return rule; }

		//INT "." INT
		public Group getGroup() { return cGroup; }

		//INT
		public RuleCall getINTTerminalRuleCall_0() { return cINTTerminalRuleCall_0; }

		//"."
		public Keyword getFullStopKeyword_1() { return cFullStopKeyword_1; }

		//INT
		public RuleCall getINTTerminalRuleCall_2() { return cINTTerminalRuleCall_2; }
	}
	
	
	private final PetriNetElements pPetriNet;
	private final ElementElements pElement;
	private final InputArcElements pInputArc;
	private final PlaceElements pPlace;
	private final TransitionElements pTransition;
	private final OutputArcElements pOutputArc;
	private final DOUBLEElements pDOUBLE;
	private final TerminalRule tXMLNS;
	private final TerminalRule tCOLON;
	private final TerminalRule tPNML;
	private final TerminalRule tVERSION;
	private final TerminalRule tXML;
	private final TerminalRule tMAXDELAY;
	private final TerminalRule tMINDELAY;
	private final TerminalRule tTYPE;
	private final TerminalRule tNET;
	private final TerminalRule tTARGET;
	private final TerminalRule tSOURCE;
	private final TerminalRule tGREATER;
	private final TerminalRule tLESS;
	private final TerminalRule tSLASH;
	private final TerminalRule tTPLACE;
	private final TerminalRule tTTRANSITION;
	private final TerminalRule tTOUTPUTARC;
	private final TerminalRule tTINPUTARC;
	private final TerminalRule tID;
	private final TerminalRule tEQUALS;
	private final TerminalRule tQUESTION;
	
	private final Grammar grammar;

	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public PetrinetDslGrammarAccess(GrammarProvider grammarProvider,
		TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
		this.pPetriNet = new PetriNetElements();
		this.pElement = new ElementElements();
		this.pInputArc = new InputArcElements();
		this.pPlace = new PlaceElements();
		this.pTransition = new TransitionElements();
		this.pOutputArc = new OutputArcElements();
		this.pDOUBLE = new DOUBLEElements();
		this.tXMLNS = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "XMLNS");
		this.tCOLON = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "COLON");
		this.tPNML = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "PNML");
		this.tVERSION = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "VERSION");
		this.tXML = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "XML");
		this.tMAXDELAY = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "MAXDELAY");
		this.tMINDELAY = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "MINDELAY");
		this.tTYPE = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "TYPE");
		this.tNET = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "NET");
		this.tTARGET = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "TARGET");
		this.tSOURCE = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "SOURCE");
		this.tGREATER = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "GREATER");
		this.tLESS = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "LESS");
		this.tSLASH = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "SLASH");
		this.tTPLACE = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "TPLACE");
		this.tTTRANSITION = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "TTRANSITION");
		this.tTOUTPUTARC = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "TOUTPUTARC");
		this.tTINPUTARC = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "TINPUTARC");
		this.tID = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "ID");
		this.tEQUALS = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "EQUALS");
		this.tQUESTION = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "QUESTION");
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("org.xtext.example.mydsl.PetrinetDsl".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	

	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//PetriNet:
	//	{PetriNet} LESS QUESTION XML VERSION EQUALS STRING QUESTION GREATER LESS PNML COLON PNML XMLNS COLON PNML EQUALS
	//	STRING GREATER LESS NET TYPE EQUALS STRING GREATER elements+=Element* LESS SLASH NET GREATER LESS SLASH PNML COLON
	//	PNML GREATER;
	public PetriNetElements getPetriNetAccess() {
		return pPetriNet;
	}
	
	public ParserRule getPetriNetRule() {
		return getPetriNetAccess().getRule();
	}

	//Element:
	//	OutputArc | InputArc | Transition | Place;
	public ElementElements getElementAccess() {
		return pElement;
	}
	
	public ParserRule getElementRule() {
		return getElementAccess().getRule();
	}

	//InputArc:
	//	LESS TINPUTARC (ID EQUALS name=STRING & SOURCE EQUALS from=[Place|STRING] & TARGET EQUALS to=[Transition|STRING])
	//	GREATER LESS SLASH TINPUTARC GREATER;
	public InputArcElements getInputArcAccess() {
		return pInputArc;
	}
	
	public ParserRule getInputArcRule() {
		return getInputArcAccess().getRule();
	}

	//Place:
	//	LESS TPLACE ID EQUALS name=STRING GREATER LESS SLASH TPLACE GREATER;
	public PlaceElements getPlaceAccess() {
		return pPlace;
	}
	
	public ParserRule getPlaceRule() {
		return getPlaceAccess().getRule();
	}

	//Transition:
	//	LESS TTRANSITION ID EQUALS name=STRING (MAXDELAY EQUALS maxDelay=DOUBLE)? (MINDELAY EQUALS minDelay=DOUBLE)? GREATER
	//	LESS SLASH TTRANSITION GREATER;
	public TransitionElements getTransitionAccess() {
		return pTransition;
	}
	
	public ParserRule getTransitionRule() {
		return getTransitionAccess().getRule();
	}

	//OutputArc:
	//	LESS TOUTPUTARC (ID EQUALS name=STRING & SOURCE EQUALS from=[Transition|STRING] & TARGET EQUALS to=[Place|STRING])
	//	GREATER LESS SLASH TOUTPUTARC GREATER;
	public OutputArcElements getOutputArcAccess() {
		return pOutputArc;
	}
	
	public ParserRule getOutputArcRule() {
		return getOutputArcAccess().getRule();
	}

	//DOUBLE returns ecore::EDouble:
	//	INT "." INT;
	public DOUBLEElements getDOUBLEAccess() {
		return pDOUBLE;
	}
	
	public ParserRule getDOUBLERule() {
		return getDOUBLEAccess().getRule();
	}

	//terminal XMLNS:
	//	"xmlns";
	public TerminalRule getXMLNSRule() {
		return tXMLNS;
	} 

	//terminal COLON:
	//	":";
	public TerminalRule getCOLONRule() {
		return tCOLON;
	} 

	//terminal PNML:
	//	"pnml";
	public TerminalRule getPNMLRule() {
		return tPNML;
	} 

	//terminal VERSION:
	//	"version";
	public TerminalRule getVERSIONRule() {
		return tVERSION;
	} 

	//terminal XML:
	//	"xml";
	public TerminalRule getXMLRule() {
		return tXML;
	} 

	//terminal MAXDELAY:
	//	"maxDelay";
	public TerminalRule getMAXDELAYRule() {
		return tMAXDELAY;
	} 

	//terminal MINDELAY:
	//	"minDelay";
	public TerminalRule getMINDELAYRule() {
		return tMINDELAY;
	} 

	//terminal TYPE:
	//	"type";
	public TerminalRule getTYPERule() {
		return tTYPE;
	} 

	//terminal NET:
	//	"net";
	public TerminalRule getNETRule() {
		return tNET;
	} 

	//terminal TARGET:
	//	"target";
	public TerminalRule getTARGETRule() {
		return tTARGET;
	} 

	//terminal SOURCE:
	//	"source";
	public TerminalRule getSOURCERule() {
		return tSOURCE;
	} 

	//terminal GREATER:
	//	">";
	public TerminalRule getGREATERRule() {
		return tGREATER;
	} 

	//terminal LESS:
	//	"<";
	public TerminalRule getLESSRule() {
		return tLESS;
	} 

	//terminal SLASH:
	//	"/";
	public TerminalRule getSLASHRule() {
		return tSLASH;
	} 

	//terminal TPLACE:
	//	"place";
	public TerminalRule getTPLACERule() {
		return tTPLACE;
	} 

	//terminal TTRANSITION:
	//	"transition";
	public TerminalRule getTTRANSITIONRule() {
		return tTTRANSITION;
	} 

	//terminal TOUTPUTARC:
	//	"outputarc";
	public TerminalRule getTOUTPUTARCRule() {
		return tTOUTPUTARC;
	} 

	//terminal TINPUTARC:
	//	"inputarc";
	public TerminalRule getTINPUTARCRule() {
		return tTINPUTARC;
	} 

	//terminal ID:
	//	"id";
	public TerminalRule getIDRule() {
		return tID;
	} 

	//terminal EQUALS:
	//	"=";
	public TerminalRule getEQUALSRule() {
		return tEQUALS;
	} 

	//terminal QUESTION:
	//	"?";
	public TerminalRule getQUESTIONRule() {
		return tQUESTION;
	} 

	//terminal INT returns ecore::EInt:
	//	"0".."9"+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	} 

	//terminal STRING:
	//	"\"" ("\\" . / * 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' * / | !("\\" | "\""))* "\"" | "\'" ("\\" .
	//	/ * 'b'|'t'|'n'|'f'|'r'|'u'|'"'|"'"|'\\' * / | !("\\" | "\'"))* "\'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	} 

	//terminal ML_COMMENT:
	//	"/ *"->"* /";
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	} 

	//terminal SL_COMMENT:
	//	"//" !("\n" | "\r")* ("\r"? "\n")?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	} 

	//terminal WS:
	//	(" " | "\t" | "\r" | "\n")+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	} 

	//terminal ANY_OTHER:
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	} 
}
