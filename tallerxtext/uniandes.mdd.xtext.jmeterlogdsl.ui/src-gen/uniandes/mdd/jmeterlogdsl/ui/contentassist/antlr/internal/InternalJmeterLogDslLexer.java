package uniandes.mdd.jmeterlogdsl.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalJmeterLogDslLexer extends Lexer {
    public static final int T__8=8;
    public static final int T__7=7;
    public static final int RULE_NEWLINE=5;
    public static final int RULE_SEPARATOR=4;
    public static final int RULE_VALELEM=6;
    public static final int EOF=-1;

    // delegates
    // delegators

    public InternalJmeterLogDslLexer() {;} 
    public InternalJmeterLogDslLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalJmeterLogDslLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g"; }

    // $ANTLR start "T__7"
    public final void mT__7() throws RecognitionException {
        try {
            int _type = T__7;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:11:6: ( 'Aggregate' )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:11:8: 'Aggregate'
            {
            match("Aggregate"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__7"

    // $ANTLR start "T__8"
    public final void mT__8() throws RecognitionException {
        try {
            int _type = T__8;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:12:6: ( 'ResponseLatency' )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:12:8: 'ResponseLatency'
            {
            match("ResponseLatency"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__8"

    // $ANTLR start "RULE_SEPARATOR"
    public final void mRULE_SEPARATOR() throws RecognitionException {
        try {
            int _type = RULE_SEPARATOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1156:16: ( ',' )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1156:18: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SEPARATOR"

    // $ANTLR start "RULE_NEWLINE"
    public final void mRULE_NEWLINE() throws RecognitionException {
        try {
            int _type = RULE_NEWLINE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1158:14: ( ( '\\r' )? '\\n' )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1158:16: ( '\\r' )? '\\n'
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1158:16: ( '\\r' )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='\r') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1158:16: '\\r'
                    {
                    match('\r'); 

                    }
                    break;

            }

            match('\n'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NEWLINE"

    // $ANTLR start "RULE_VALELEM"
    public final void mRULE_VALELEM() throws RecognitionException {
        try {
            int _type = RULE_VALELEM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1160:14: ( ( '\"' ( . )+ '\"' | (~ ( ( '\"' | ',' | '\\r' | '\\n' ) ) )+ ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1160:16: ( '\"' ( . )+ '\"' | (~ ( ( '\"' | ',' | '\\r' | '\\n' ) ) )+ )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1160:16: ( '\"' ( . )+ '\"' | (~ ( ( '\"' | ',' | '\\r' | '\\n' ) ) )+ )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='\"') ) {
                alt4=1;
            }
            else if ( ((LA4_0>='\u0000' && LA4_0<='\t')||(LA4_0>='\u000B' && LA4_0<='\f')||(LA4_0>='\u000E' && LA4_0<='!')||(LA4_0>='#' && LA4_0<='+')||(LA4_0>='-' && LA4_0<='\uFFFF')) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1160:17: '\"' ( . )+ '\"'
                    {
                    match('\"'); 
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1160:21: ( . )+
                    int cnt2=0;
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0=='\"') ) {
                            alt2=2;
                        }
                        else if ( ((LA2_0>='\u0000' && LA2_0<='!')||(LA2_0>='#' && LA2_0<='\uFFFF')) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1160:21: .
                    	    {
                    	    matchAny(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt2 >= 1 ) break loop2;
                                EarlyExitException eee =
                                    new EarlyExitException(2, input);
                                throw eee;
                        }
                        cnt2++;
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1160:28: (~ ( ( '\"' | ',' | '\\r' | '\\n' ) ) )+
                    {
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1160:28: (~ ( ( '\"' | ',' | '\\r' | '\\n' ) ) )+
                    int cnt3=0;
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( ((LA3_0>='\u0000' && LA3_0<='\t')||(LA3_0>='\u000B' && LA3_0<='\f')||(LA3_0>='\u000E' && LA3_0<='!')||(LA3_0>='#' && LA3_0<='+')||(LA3_0>='-' && LA3_0<='\uFFFF')) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1160:28: ~ ( ( '\"' | ',' | '\\r' | '\\n' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='+')||(input.LA(1)>='-' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt3 >= 1 ) break loop3;
                                EarlyExitException eee =
                                    new EarlyExitException(3, input);
                                throw eee;
                        }
                        cnt3++;
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_VALELEM"

    public void mTokens() throws RecognitionException {
        // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1:8: ( T__7 | T__8 | RULE_SEPARATOR | RULE_NEWLINE | RULE_VALELEM )
        int alt5=5;
        alt5 = dfa5.predict(input);
        switch (alt5) {
            case 1 :
                // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1:10: T__7
                {
                mT__7(); 

                }
                break;
            case 2 :
                // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1:15: T__8
                {
                mT__8(); 

                }
                break;
            case 3 :
                // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1:20: RULE_SEPARATOR
                {
                mRULE_SEPARATOR(); 

                }
                break;
            case 4 :
                // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1:35: RULE_NEWLINE
                {
                mRULE_NEWLINE(); 

                }
                break;
            case 5 :
                // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1:48: RULE_VALELEM
                {
                mRULE_VALELEM(); 

                }
                break;

        }

    }


    protected DFA5 dfa5 = new DFA5(this);
    static final String DFA5_eotS =
        "\1\uffff\2\5\3\uffff\16\5\1\26\1\5\1\uffff\5\5\1\35\1\uffff";
    static final String DFA5_eofS =
        "\36\uffff";
    static final String DFA5_minS =
        "\1\0\1\147\1\145\3\uffff\1\147\1\163\1\162\1\160\1\145\1\157\1"+
        "\147\1\156\1\141\1\163\1\164\2\145\1\114\1\0\1\141\1\uffff\1\164"+
        "\1\145\1\156\1\143\1\171\1\0\1\uffff";
    static final String DFA5_maxS =
        "\1\uffff\1\147\1\145\3\uffff\1\147\1\163\1\162\1\160\1\145\1\157"+
        "\1\147\1\156\1\141\1\163\1\164\2\145\1\114\1\uffff\1\141\1\uffff"+
        "\1\164\1\145\1\156\1\143\1\171\1\uffff\1\uffff";
    static final String DFA5_acceptS =
        "\3\uffff\1\3\1\4\1\5\20\uffff\1\1\6\uffff\1\2";
    static final String DFA5_specialS =
        "\1\1\23\uffff\1\2\7\uffff\1\0\1\uffff}>";
    static final String[] DFA5_transitionS = {
            "\12\5\1\4\2\5\1\4\36\5\1\3\24\5\1\1\20\5\1\2\uffad\5",
            "\1\6",
            "\1\7",
            "",
            "",
            "",
            "\1\10",
            "\1\11",
            "\1\12",
            "\1\13",
            "\1\14",
            "\1\15",
            "\1\16",
            "\1\17",
            "\1\20",
            "\1\21",
            "\1\22",
            "\1\23",
            "\1\24",
            "\1\25",
            "\12\5\1\uffff\2\5\1\uffff\24\5\1\uffff\11\5\1\uffff\uffd3"+
            "\5",
            "\1\27",
            "",
            "\1\30",
            "\1\31",
            "\1\32",
            "\1\33",
            "\1\34",
            "\12\5\1\uffff\2\5\1\uffff\24\5\1\uffff\11\5\1\uffff\uffd3"+
            "\5",
            ""
    };

    static final short[] DFA5_eot = DFA.unpackEncodedString(DFA5_eotS);
    static final short[] DFA5_eof = DFA.unpackEncodedString(DFA5_eofS);
    static final char[] DFA5_min = DFA.unpackEncodedStringToUnsignedChars(DFA5_minS);
    static final char[] DFA5_max = DFA.unpackEncodedStringToUnsignedChars(DFA5_maxS);
    static final short[] DFA5_accept = DFA.unpackEncodedString(DFA5_acceptS);
    static final short[] DFA5_special = DFA.unpackEncodedString(DFA5_specialS);
    static final short[][] DFA5_transition;

    static {
        int numStates = DFA5_transitionS.length;
        DFA5_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA5_transition[i] = DFA.unpackEncodedString(DFA5_transitionS[i]);
        }
    }

    static class DFA5 extends DFA {

        public DFA5(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 5;
            this.eot = DFA5_eot;
            this.eof = DFA5_eof;
            this.min = DFA5_min;
            this.max = DFA5_max;
            this.accept = DFA5_accept;
            this.special = DFA5_special;
            this.transition = DFA5_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__7 | T__8 | RULE_SEPARATOR | RULE_NEWLINE | RULE_VALELEM );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA5_28 = input.LA(1);

                        s = -1;
                        if ( ((LA5_28>='\u0000' && LA5_28<='\t')||(LA5_28>='\u000B' && LA5_28<='\f')||(LA5_28>='\u000E' && LA5_28<='!')||(LA5_28>='#' && LA5_28<='+')||(LA5_28>='-' && LA5_28<='\uFFFF')) ) {s = 5;}

                        else s = 29;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA5_0 = input.LA(1);

                        s = -1;
                        if ( (LA5_0=='A') ) {s = 1;}

                        else if ( (LA5_0=='R') ) {s = 2;}

                        else if ( (LA5_0==',') ) {s = 3;}

                        else if ( (LA5_0=='\n'||LA5_0=='\r') ) {s = 4;}

                        else if ( ((LA5_0>='\u0000' && LA5_0<='\t')||(LA5_0>='\u000B' && LA5_0<='\f')||(LA5_0>='\u000E' && LA5_0<='+')||(LA5_0>='-' && LA5_0<='@')||(LA5_0>='B' && LA5_0<='Q')||(LA5_0>='S' && LA5_0<='\uFFFF')) ) {s = 5;}

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA5_20 = input.LA(1);

                        s = -1;
                        if ( ((LA5_20>='\u0000' && LA5_20<='\t')||(LA5_20>='\u000B' && LA5_20<='\f')||(LA5_20>='\u000E' && LA5_20<='!')||(LA5_20>='#' && LA5_20<='+')||(LA5_20>='-' && LA5_20<='\uFFFF')) ) {s = 5;}

                        else s = 22;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 5, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}