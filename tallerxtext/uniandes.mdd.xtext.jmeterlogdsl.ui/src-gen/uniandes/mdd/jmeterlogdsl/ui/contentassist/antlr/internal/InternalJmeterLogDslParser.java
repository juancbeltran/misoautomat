package uniandes.mdd.jmeterlogdsl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import uniandes.mdd.jmeterlogdsl.services.JmeterLogDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalJmeterLogDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_SEPARATOR", "RULE_NEWLINE", "RULE_VALELEM", "'Aggregate'", "'ResponseLatency'"
    };
    public static final int T__8=8;
    public static final int T__7=7;
    public static final int RULE_NEWLINE=5;
    public static final int RULE_SEPARATOR=4;
    public static final int RULE_VALELEM=6;
    public static final int EOF=-1;

    // delegates
    // delegators


        public InternalJmeterLogDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalJmeterLogDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalJmeterLogDslParser.tokenNames; }
    public String getGrammarFileName() { return "../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g"; }


     
     	private JmeterLogDslGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(JmeterLogDslGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleReporte"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:60:1: entryRuleReporte : ruleReporte EOF ;
    public final void entryRuleReporte() throws RecognitionException {
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:61:1: ( ruleReporte EOF )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:62:1: ruleReporte EOF
            {
             before(grammarAccess.getReporteRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleReporte_in_entryRuleReporte61);
            ruleReporte();

            state._fsp--;

             after(grammarAccess.getReporteRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleReporte68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReporte"


    // $ANTLR start "ruleReporte"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:69:1: ruleReporte : ( ( rule__Reporte__Group__0 ) ) ;
    public final void ruleReporte() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:73:2: ( ( ( rule__Reporte__Group__0 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:74:1: ( ( rule__Reporte__Group__0 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:74:1: ( ( rule__Reporte__Group__0 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:75:1: ( rule__Reporte__Group__0 )
            {
             before(grammarAccess.getReporteAccess().getGroup()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:76:1: ( rule__Reporte__Group__0 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:76:2: rule__Reporte__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__0_in_ruleReporte94);
            rule__Reporte__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReporteAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReporte"


    // $ANTLR start "entryRuleRegistro"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:88:1: entryRuleRegistro : ruleRegistro EOF ;
    public final void entryRuleRegistro() throws RecognitionException {
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:89:1: ( ruleRegistro EOF )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:90:1: ruleRegistro EOF
            {
             before(grammarAccess.getRegistroRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleRegistro_in_entryRuleRegistro121);
            ruleRegistro();

            state._fsp--;

             after(grammarAccess.getRegistroRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleRegistro128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRegistro"


    // $ANTLR start "ruleRegistro"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:97:1: ruleRegistro : ( ( rule__Registro__Group__0 ) ) ;
    public final void ruleRegistro() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:101:2: ( ( ( rule__Registro__Group__0 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:102:1: ( ( rule__Registro__Group__0 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:102:1: ( ( rule__Registro__Group__0 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:103:1: ( rule__Registro__Group__0 )
            {
             before(grammarAccess.getRegistroAccess().getGroup()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:104:1: ( rule__Registro__Group__0 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:104:2: rule__Registro__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Registro__Group__0_in_ruleRegistro154);
            rule__Registro__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRegistroAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRegistro"


    // $ANTLR start "entryRuleEncabezado"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:116:1: entryRuleEncabezado : ruleEncabezado EOF ;
    public final void entryRuleEncabezado() throws RecognitionException {
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:117:1: ( ruleEncabezado EOF )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:118:1: ruleEncabezado EOF
            {
             before(grammarAccess.getEncabezadoRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleEncabezado_in_entryRuleEncabezado181);
            ruleEncabezado();

            state._fsp--;

             after(grammarAccess.getEncabezadoRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleEncabezado188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEncabezado"


    // $ANTLR start "ruleEncabezado"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:125:1: ruleEncabezado : ( ( rule__Encabezado__Group__0 ) ) ;
    public final void ruleEncabezado() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:129:2: ( ( ( rule__Encabezado__Group__0 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:130:1: ( ( rule__Encabezado__Group__0 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:130:1: ( ( rule__Encabezado__Group__0 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:131:1: ( rule__Encabezado__Group__0 )
            {
             before(grammarAccess.getEncabezadoAccess().getGroup()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:132:1: ( rule__Encabezado__Group__0 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:132:2: rule__Encabezado__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__Group__0_in_ruleEncabezado214);
            rule__Encabezado__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEncabezadoAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEncabezado"


    // $ANTLR start "entryRuleElemento"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:144:1: entryRuleElemento : ruleElemento EOF ;
    public final void entryRuleElemento() throws RecognitionException {
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:145:1: ( ruleElemento EOF )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:146:1: ruleElemento EOF
            {
             before(grammarAccess.getElementoRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleElemento_in_entryRuleElemento241);
            ruleElemento();

            state._fsp--;

             after(grammarAccess.getElementoRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleElemento248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleElemento"


    // $ANTLR start "ruleElemento"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:153:1: ruleElemento : ( ( rule__Elemento__Group__0 ) ) ;
    public final void ruleElemento() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:157:2: ( ( ( rule__Elemento__Group__0 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:158:1: ( ( rule__Elemento__Group__0 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:158:1: ( ( rule__Elemento__Group__0 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:159:1: ( rule__Elemento__Group__0 )
            {
             before(grammarAccess.getElementoAccess().getGroup()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:160:1: ( rule__Elemento__Group__0 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:160:2: rule__Elemento__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Elemento__Group__0_in_ruleElemento274);
            rule__Elemento__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getElementoAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleElemento"


    // $ANTLR start "entryRuleDescripcionColumna"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:172:1: entryRuleDescripcionColumna : ruleDescripcionColumna EOF ;
    public final void entryRuleDescripcionColumna() throws RecognitionException {
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:173:1: ( ruleDescripcionColumna EOF )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:174:1: ruleDescripcionColumna EOF
            {
             before(grammarAccess.getDescripcionColumnaRule()); 
            pushFollow(FollowSets000.FOLLOW_ruleDescripcionColumna_in_entryRuleDescripcionColumna301);
            ruleDescripcionColumna();

            state._fsp--;

             after(grammarAccess.getDescripcionColumnaRule()); 
            match(input,EOF,FollowSets000.FOLLOW_EOF_in_entryRuleDescripcionColumna308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDescripcionColumna"


    // $ANTLR start "ruleDescripcionColumna"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:181:1: ruleDescripcionColumna : ( ( rule__DescripcionColumna__Group__0 ) ) ;
    public final void ruleDescripcionColumna() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:185:2: ( ( ( rule__DescripcionColumna__Group__0 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:186:1: ( ( rule__DescripcionColumna__Group__0 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:186:1: ( ( rule__DescripcionColumna__Group__0 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:187:1: ( rule__DescripcionColumna__Group__0 )
            {
             before(grammarAccess.getDescripcionColumnaAccess().getGroup()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:188:1: ( rule__DescripcionColumna__Group__0 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:188:2: rule__DescripcionColumna__Group__0
            {
            pushFollow(FollowSets000.FOLLOW_rule__DescripcionColumna__Group__0_in_ruleDescripcionColumna334);
            rule__DescripcionColumna__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDescripcionColumnaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDescripcionColumna"


    // $ANTLR start "ruleTipoReporte"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:201:1: ruleTipoReporte : ( ( rule__TipoReporte__Alternatives ) ) ;
    public final void ruleTipoReporte() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:205:1: ( ( ( rule__TipoReporte__Alternatives ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:206:1: ( ( rule__TipoReporte__Alternatives ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:206:1: ( ( rule__TipoReporte__Alternatives ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:207:1: ( rule__TipoReporte__Alternatives )
            {
             before(grammarAccess.getTipoReporteAccess().getAlternatives()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:208:1: ( rule__TipoReporte__Alternatives )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:208:2: rule__TipoReporte__Alternatives
            {
            pushFollow(FollowSets000.FOLLOW_rule__TipoReporte__Alternatives_in_ruleTipoReporte371);
            rule__TipoReporte__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTipoReporteAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTipoReporte"


    // $ANTLR start "rule__TipoReporte__Alternatives"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:219:1: rule__TipoReporte__Alternatives : ( ( ( 'Aggregate' ) ) | ( ( 'ResponseLatency' ) ) );
    public final void rule__TipoReporte__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:223:1: ( ( ( 'Aggregate' ) ) | ( ( 'ResponseLatency' ) ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==7) ) {
                alt1=1;
            }
            else if ( (LA1_0==8) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:224:1: ( ( 'Aggregate' ) )
                    {
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:224:1: ( ( 'Aggregate' ) )
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:225:1: ( 'Aggregate' )
                    {
                     before(grammarAccess.getTipoReporteAccess().getAggregateEnumLiteralDeclaration_0()); 
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:226:1: ( 'Aggregate' )
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:226:3: 'Aggregate'
                    {
                    match(input,7,FollowSets000.FOLLOW_7_in_rule__TipoReporte__Alternatives407); 

                    }

                     after(grammarAccess.getTipoReporteAccess().getAggregateEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:231:6: ( ( 'ResponseLatency' ) )
                    {
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:231:6: ( ( 'ResponseLatency' ) )
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:232:1: ( 'ResponseLatency' )
                    {
                     before(grammarAccess.getTipoReporteAccess().getResponseLatencyEnumLiteralDeclaration_1()); 
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:233:1: ( 'ResponseLatency' )
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:233:3: 'ResponseLatency'
                    {
                    match(input,8,FollowSets000.FOLLOW_8_in_rule__TipoReporte__Alternatives428); 

                    }

                     after(grammarAccess.getTipoReporteAccess().getResponseLatencyEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TipoReporte__Alternatives"


    // $ANTLR start "rule__Reporte__Group__0"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:245:1: rule__Reporte__Group__0 : rule__Reporte__Group__0__Impl rule__Reporte__Group__1 ;
    public final void rule__Reporte__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:249:1: ( rule__Reporte__Group__0__Impl rule__Reporte__Group__1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:250:2: rule__Reporte__Group__0__Impl rule__Reporte__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__0__Impl_in_rule__Reporte__Group__0461);
            rule__Reporte__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__1_in_rule__Reporte__Group__0464);
            rule__Reporte__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__0"


    // $ANTLR start "rule__Reporte__Group__0__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:257:1: rule__Reporte__Group__0__Impl : ( ( rule__Reporte__TipoReporteAssignment_0 ) ) ;
    public final void rule__Reporte__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:261:1: ( ( ( rule__Reporte__TipoReporteAssignment_0 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:262:1: ( ( rule__Reporte__TipoReporteAssignment_0 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:262:1: ( ( rule__Reporte__TipoReporteAssignment_0 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:263:1: ( rule__Reporte__TipoReporteAssignment_0 )
            {
             before(grammarAccess.getReporteAccess().getTipoReporteAssignment_0()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:264:1: ( rule__Reporte__TipoReporteAssignment_0 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:264:2: rule__Reporte__TipoReporteAssignment_0
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__TipoReporteAssignment_0_in_rule__Reporte__Group__0__Impl491);
            rule__Reporte__TipoReporteAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getReporteAccess().getTipoReporteAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__0__Impl"


    // $ANTLR start "rule__Reporte__Group__1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:274:1: rule__Reporte__Group__1 : rule__Reporte__Group__1__Impl rule__Reporte__Group__2 ;
    public final void rule__Reporte__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:278:1: ( rule__Reporte__Group__1__Impl rule__Reporte__Group__2 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:279:2: rule__Reporte__Group__1__Impl rule__Reporte__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__1__Impl_in_rule__Reporte__Group__1521);
            rule__Reporte__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__2_in_rule__Reporte__Group__1524);
            rule__Reporte__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__1"


    // $ANTLR start "rule__Reporte__Group__1__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:286:1: rule__Reporte__Group__1__Impl : ( RULE_SEPARATOR ) ;
    public final void rule__Reporte__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:290:1: ( ( RULE_SEPARATOR ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:291:1: ( RULE_SEPARATOR )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:291:1: ( RULE_SEPARATOR )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:292:1: RULE_SEPARATOR
            {
             before(grammarAccess.getReporteAccess().getSEPARATORTerminalRuleCall_1()); 
            match(input,RULE_SEPARATOR,FollowSets000.FOLLOW_RULE_SEPARATOR_in_rule__Reporte__Group__1__Impl551); 
             after(grammarAccess.getReporteAccess().getSEPARATORTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__1__Impl"


    // $ANTLR start "rule__Reporte__Group__2"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:303:1: rule__Reporte__Group__2 : rule__Reporte__Group__2__Impl rule__Reporte__Group__3 ;
    public final void rule__Reporte__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:307:1: ( rule__Reporte__Group__2__Impl rule__Reporte__Group__3 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:308:2: rule__Reporte__Group__2__Impl rule__Reporte__Group__3
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__2__Impl_in_rule__Reporte__Group__2580);
            rule__Reporte__Group__2__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__3_in_rule__Reporte__Group__2583);
            rule__Reporte__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__2"


    // $ANTLR start "rule__Reporte__Group__2__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:315:1: rule__Reporte__Group__2__Impl : ( ( rule__Reporte__ArquitecturaAssignment_2 ) ) ;
    public final void rule__Reporte__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:319:1: ( ( ( rule__Reporte__ArquitecturaAssignment_2 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:320:1: ( ( rule__Reporte__ArquitecturaAssignment_2 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:320:1: ( ( rule__Reporte__ArquitecturaAssignment_2 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:321:1: ( rule__Reporte__ArquitecturaAssignment_2 )
            {
             before(grammarAccess.getReporteAccess().getArquitecturaAssignment_2()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:322:1: ( rule__Reporte__ArquitecturaAssignment_2 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:322:2: rule__Reporte__ArquitecturaAssignment_2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__ArquitecturaAssignment_2_in_rule__Reporte__Group__2__Impl610);
            rule__Reporte__ArquitecturaAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getReporteAccess().getArquitecturaAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__2__Impl"


    // $ANTLR start "rule__Reporte__Group__3"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:332:1: rule__Reporte__Group__3 : rule__Reporte__Group__3__Impl rule__Reporte__Group__4 ;
    public final void rule__Reporte__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:336:1: ( rule__Reporte__Group__3__Impl rule__Reporte__Group__4 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:337:2: rule__Reporte__Group__3__Impl rule__Reporte__Group__4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__3__Impl_in_rule__Reporte__Group__3640);
            rule__Reporte__Group__3__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__4_in_rule__Reporte__Group__3643);
            rule__Reporte__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__3"


    // $ANTLR start "rule__Reporte__Group__3__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:344:1: rule__Reporte__Group__3__Impl : ( RULE_NEWLINE ) ;
    public final void rule__Reporte__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:348:1: ( ( RULE_NEWLINE ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:349:1: ( RULE_NEWLINE )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:349:1: ( RULE_NEWLINE )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:350:1: RULE_NEWLINE
            {
             before(grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_3()); 
            match(input,RULE_NEWLINE,FollowSets000.FOLLOW_RULE_NEWLINE_in_rule__Reporte__Group__3__Impl670); 
             after(grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__3__Impl"


    // $ANTLR start "rule__Reporte__Group__4"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:361:1: rule__Reporte__Group__4 : rule__Reporte__Group__4__Impl rule__Reporte__Group__5 ;
    public final void rule__Reporte__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:365:1: ( rule__Reporte__Group__4__Impl rule__Reporte__Group__5 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:366:2: rule__Reporte__Group__4__Impl rule__Reporte__Group__5
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__4__Impl_in_rule__Reporte__Group__4699);
            rule__Reporte__Group__4__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__5_in_rule__Reporte__Group__4702);
            rule__Reporte__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__4"


    // $ANTLR start "rule__Reporte__Group__4__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:373:1: rule__Reporte__Group__4__Impl : ( ( rule__Reporte__EncabezadoAssignment_4 ) ) ;
    public final void rule__Reporte__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:377:1: ( ( ( rule__Reporte__EncabezadoAssignment_4 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:378:1: ( ( rule__Reporte__EncabezadoAssignment_4 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:378:1: ( ( rule__Reporte__EncabezadoAssignment_4 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:379:1: ( rule__Reporte__EncabezadoAssignment_4 )
            {
             before(grammarAccess.getReporteAccess().getEncabezadoAssignment_4()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:380:1: ( rule__Reporte__EncabezadoAssignment_4 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:380:2: rule__Reporte__EncabezadoAssignment_4
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__EncabezadoAssignment_4_in_rule__Reporte__Group__4__Impl729);
            rule__Reporte__EncabezadoAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getReporteAccess().getEncabezadoAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__4__Impl"


    // $ANTLR start "rule__Reporte__Group__5"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:390:1: rule__Reporte__Group__5 : rule__Reporte__Group__5__Impl rule__Reporte__Group__6 ;
    public final void rule__Reporte__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:394:1: ( rule__Reporte__Group__5__Impl rule__Reporte__Group__6 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:395:2: rule__Reporte__Group__5__Impl rule__Reporte__Group__6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__5__Impl_in_rule__Reporte__Group__5759);
            rule__Reporte__Group__5__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__6_in_rule__Reporte__Group__5762);
            rule__Reporte__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__5"


    // $ANTLR start "rule__Reporte__Group__5__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:402:1: rule__Reporte__Group__5__Impl : ( RULE_NEWLINE ) ;
    public final void rule__Reporte__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:406:1: ( ( RULE_NEWLINE ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:407:1: ( RULE_NEWLINE )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:407:1: ( RULE_NEWLINE )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:408:1: RULE_NEWLINE
            {
             before(grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_5()); 
            match(input,RULE_NEWLINE,FollowSets000.FOLLOW_RULE_NEWLINE_in_rule__Reporte__Group__5__Impl789); 
             after(grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__5__Impl"


    // $ANTLR start "rule__Reporte__Group__6"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:419:1: rule__Reporte__Group__6 : rule__Reporte__Group__6__Impl rule__Reporte__Group__7 ;
    public final void rule__Reporte__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:423:1: ( rule__Reporte__Group__6__Impl rule__Reporte__Group__7 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:424:2: rule__Reporte__Group__6__Impl rule__Reporte__Group__7
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__6__Impl_in_rule__Reporte__Group__6818);
            rule__Reporte__Group__6__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__7_in_rule__Reporte__Group__6821);
            rule__Reporte__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__6"


    // $ANTLR start "rule__Reporte__Group__6__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:431:1: rule__Reporte__Group__6__Impl : ( ( rule__Reporte__RegistrosAssignment_6 ) ) ;
    public final void rule__Reporte__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:435:1: ( ( ( rule__Reporte__RegistrosAssignment_6 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:436:1: ( ( rule__Reporte__RegistrosAssignment_6 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:436:1: ( ( rule__Reporte__RegistrosAssignment_6 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:437:1: ( rule__Reporte__RegistrosAssignment_6 )
            {
             before(grammarAccess.getReporteAccess().getRegistrosAssignment_6()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:438:1: ( rule__Reporte__RegistrosAssignment_6 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:438:2: rule__Reporte__RegistrosAssignment_6
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__RegistrosAssignment_6_in_rule__Reporte__Group__6__Impl848);
            rule__Reporte__RegistrosAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getReporteAccess().getRegistrosAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__6__Impl"


    // $ANTLR start "rule__Reporte__Group__7"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:448:1: rule__Reporte__Group__7 : rule__Reporte__Group__7__Impl ;
    public final void rule__Reporte__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:452:1: ( rule__Reporte__Group__7__Impl )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:453:2: rule__Reporte__Group__7__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group__7__Impl_in_rule__Reporte__Group__7878);
            rule__Reporte__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__7"


    // $ANTLR start "rule__Reporte__Group__7__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:459:1: rule__Reporte__Group__7__Impl : ( ( rule__Reporte__Group_7__0 )* ) ;
    public final void rule__Reporte__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:463:1: ( ( ( rule__Reporte__Group_7__0 )* ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:464:1: ( ( rule__Reporte__Group_7__0 )* )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:464:1: ( ( rule__Reporte__Group_7__0 )* )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:465:1: ( rule__Reporte__Group_7__0 )*
            {
             before(grammarAccess.getReporteAccess().getGroup_7()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:466:1: ( rule__Reporte__Group_7__0 )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_NEWLINE) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:466:2: rule__Reporte__Group_7__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group_7__0_in_rule__Reporte__Group__7__Impl905);
            	    rule__Reporte__Group_7__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getReporteAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group__7__Impl"


    // $ANTLR start "rule__Reporte__Group_7__0"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:492:1: rule__Reporte__Group_7__0 : rule__Reporte__Group_7__0__Impl rule__Reporte__Group_7__1 ;
    public final void rule__Reporte__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:496:1: ( rule__Reporte__Group_7__0__Impl rule__Reporte__Group_7__1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:497:2: rule__Reporte__Group_7__0__Impl rule__Reporte__Group_7__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group_7__0__Impl_in_rule__Reporte__Group_7__0952);
            rule__Reporte__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group_7__1_in_rule__Reporte__Group_7__0955);
            rule__Reporte__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group_7__0"


    // $ANTLR start "rule__Reporte__Group_7__0__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:504:1: rule__Reporte__Group_7__0__Impl : ( RULE_NEWLINE ) ;
    public final void rule__Reporte__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:508:1: ( ( RULE_NEWLINE ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:509:1: ( RULE_NEWLINE )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:509:1: ( RULE_NEWLINE )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:510:1: RULE_NEWLINE
            {
             before(grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_7_0()); 
            match(input,RULE_NEWLINE,FollowSets000.FOLLOW_RULE_NEWLINE_in_rule__Reporte__Group_7__0__Impl982); 
             after(grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group_7__0__Impl"


    // $ANTLR start "rule__Reporte__Group_7__1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:521:1: rule__Reporte__Group_7__1 : rule__Reporte__Group_7__1__Impl ;
    public final void rule__Reporte__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:525:1: ( rule__Reporte__Group_7__1__Impl )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:526:2: rule__Reporte__Group_7__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__Group_7__1__Impl_in_rule__Reporte__Group_7__11011);
            rule__Reporte__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group_7__1"


    // $ANTLR start "rule__Reporte__Group_7__1__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:532:1: rule__Reporte__Group_7__1__Impl : ( ( rule__Reporte__RegistrosAssignment_7_1 ) ) ;
    public final void rule__Reporte__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:536:1: ( ( ( rule__Reporte__RegistrosAssignment_7_1 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:537:1: ( ( rule__Reporte__RegistrosAssignment_7_1 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:537:1: ( ( rule__Reporte__RegistrosAssignment_7_1 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:538:1: ( rule__Reporte__RegistrosAssignment_7_1 )
            {
             before(grammarAccess.getReporteAccess().getRegistrosAssignment_7_1()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:539:1: ( rule__Reporte__RegistrosAssignment_7_1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:539:2: rule__Reporte__RegistrosAssignment_7_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Reporte__RegistrosAssignment_7_1_in_rule__Reporte__Group_7__1__Impl1038);
            rule__Reporte__RegistrosAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getReporteAccess().getRegistrosAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__Group_7__1__Impl"


    // $ANTLR start "rule__Registro__Group__0"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:553:1: rule__Registro__Group__0 : rule__Registro__Group__0__Impl rule__Registro__Group__1 ;
    public final void rule__Registro__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:557:1: ( rule__Registro__Group__0__Impl rule__Registro__Group__1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:558:2: rule__Registro__Group__0__Impl rule__Registro__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Registro__Group__0__Impl_in_rule__Registro__Group__01072);
            rule__Registro__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Registro__Group__1_in_rule__Registro__Group__01075);
            rule__Registro__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__Group__0"


    // $ANTLR start "rule__Registro__Group__0__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:565:1: rule__Registro__Group__0__Impl : ( () ) ;
    public final void rule__Registro__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:569:1: ( ( () ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:570:1: ( () )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:570:1: ( () )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:571:1: ()
            {
             before(grammarAccess.getRegistroAccess().getRegistroAction_0()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:572:1: ()
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:574:1: 
            {
            }

             after(grammarAccess.getRegistroAccess().getRegistroAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__Group__0__Impl"


    // $ANTLR start "rule__Registro__Group__1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:584:1: rule__Registro__Group__1 : rule__Registro__Group__1__Impl rule__Registro__Group__2 ;
    public final void rule__Registro__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:588:1: ( rule__Registro__Group__1__Impl rule__Registro__Group__2 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:589:2: rule__Registro__Group__1__Impl rule__Registro__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Registro__Group__1__Impl_in_rule__Registro__Group__11133);
            rule__Registro__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Registro__Group__2_in_rule__Registro__Group__11136);
            rule__Registro__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__Group__1"


    // $ANTLR start "rule__Registro__Group__1__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:596:1: rule__Registro__Group__1__Impl : ( ( rule__Registro__ElementosAssignment_1 )? ) ;
    public final void rule__Registro__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:600:1: ( ( ( rule__Registro__ElementosAssignment_1 )? ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:601:1: ( ( rule__Registro__ElementosAssignment_1 )? )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:601:1: ( ( rule__Registro__ElementosAssignment_1 )? )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:602:1: ( rule__Registro__ElementosAssignment_1 )?
            {
             before(grammarAccess.getRegistroAccess().getElementosAssignment_1()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:603:1: ( rule__Registro__ElementosAssignment_1 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_VALELEM) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:603:2: rule__Registro__ElementosAssignment_1
                    {
                    pushFollow(FollowSets000.FOLLOW_rule__Registro__ElementosAssignment_1_in_rule__Registro__Group__1__Impl1163);
                    rule__Registro__ElementosAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRegistroAccess().getElementosAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__Group__1__Impl"


    // $ANTLR start "rule__Registro__Group__2"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:613:1: rule__Registro__Group__2 : rule__Registro__Group__2__Impl ;
    public final void rule__Registro__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:617:1: ( rule__Registro__Group__2__Impl )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:618:2: rule__Registro__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Registro__Group__2__Impl_in_rule__Registro__Group__21194);
            rule__Registro__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__Group__2"


    // $ANTLR start "rule__Registro__Group__2__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:624:1: rule__Registro__Group__2__Impl : ( ( rule__Registro__Group_2__0 )* ) ;
    public final void rule__Registro__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:628:1: ( ( ( rule__Registro__Group_2__0 )* ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:629:1: ( ( rule__Registro__Group_2__0 )* )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:629:1: ( ( rule__Registro__Group_2__0 )* )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:630:1: ( rule__Registro__Group_2__0 )*
            {
             before(grammarAccess.getRegistroAccess().getGroup_2()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:631:1: ( rule__Registro__Group_2__0 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_SEPARATOR) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:631:2: rule__Registro__Group_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Registro__Group_2__0_in_rule__Registro__Group__2__Impl1221);
            	    rule__Registro__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getRegistroAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__Group__2__Impl"


    // $ANTLR start "rule__Registro__Group_2__0"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:647:1: rule__Registro__Group_2__0 : rule__Registro__Group_2__0__Impl rule__Registro__Group_2__1 ;
    public final void rule__Registro__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:651:1: ( rule__Registro__Group_2__0__Impl rule__Registro__Group_2__1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:652:2: rule__Registro__Group_2__0__Impl rule__Registro__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Registro__Group_2__0__Impl_in_rule__Registro__Group_2__01258);
            rule__Registro__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Registro__Group_2__1_in_rule__Registro__Group_2__01261);
            rule__Registro__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__Group_2__0"


    // $ANTLR start "rule__Registro__Group_2__0__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:659:1: rule__Registro__Group_2__0__Impl : ( RULE_SEPARATOR ) ;
    public final void rule__Registro__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:663:1: ( ( RULE_SEPARATOR ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:664:1: ( RULE_SEPARATOR )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:664:1: ( RULE_SEPARATOR )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:665:1: RULE_SEPARATOR
            {
             before(grammarAccess.getRegistroAccess().getSEPARATORTerminalRuleCall_2_0()); 
            match(input,RULE_SEPARATOR,FollowSets000.FOLLOW_RULE_SEPARATOR_in_rule__Registro__Group_2__0__Impl1288); 
             after(grammarAccess.getRegistroAccess().getSEPARATORTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__Group_2__0__Impl"


    // $ANTLR start "rule__Registro__Group_2__1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:676:1: rule__Registro__Group_2__1 : rule__Registro__Group_2__1__Impl ;
    public final void rule__Registro__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:680:1: ( rule__Registro__Group_2__1__Impl )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:681:2: rule__Registro__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Registro__Group_2__1__Impl_in_rule__Registro__Group_2__11317);
            rule__Registro__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__Group_2__1"


    // $ANTLR start "rule__Registro__Group_2__1__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:687:1: rule__Registro__Group_2__1__Impl : ( ( rule__Registro__ElementosAssignment_2_1 ) ) ;
    public final void rule__Registro__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:691:1: ( ( ( rule__Registro__ElementosAssignment_2_1 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:692:1: ( ( rule__Registro__ElementosAssignment_2_1 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:692:1: ( ( rule__Registro__ElementosAssignment_2_1 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:693:1: ( rule__Registro__ElementosAssignment_2_1 )
            {
             before(grammarAccess.getRegistroAccess().getElementosAssignment_2_1()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:694:1: ( rule__Registro__ElementosAssignment_2_1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:694:2: rule__Registro__ElementosAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Registro__ElementosAssignment_2_1_in_rule__Registro__Group_2__1__Impl1344);
            rule__Registro__ElementosAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getRegistroAccess().getElementosAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__Group_2__1__Impl"


    // $ANTLR start "rule__Encabezado__Group__0"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:708:1: rule__Encabezado__Group__0 : rule__Encabezado__Group__0__Impl rule__Encabezado__Group__1 ;
    public final void rule__Encabezado__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:712:1: ( rule__Encabezado__Group__0__Impl rule__Encabezado__Group__1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:713:2: rule__Encabezado__Group__0__Impl rule__Encabezado__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__Group__0__Impl_in_rule__Encabezado__Group__01378);
            rule__Encabezado__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__Group__1_in_rule__Encabezado__Group__01381);
            rule__Encabezado__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__Group__0"


    // $ANTLR start "rule__Encabezado__Group__0__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:720:1: rule__Encabezado__Group__0__Impl : ( () ) ;
    public final void rule__Encabezado__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:724:1: ( ( () ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:725:1: ( () )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:725:1: ( () )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:726:1: ()
            {
             before(grammarAccess.getEncabezadoAccess().getEncabezadoAction_0()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:727:1: ()
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:729:1: 
            {
            }

             after(grammarAccess.getEncabezadoAccess().getEncabezadoAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__Group__0__Impl"


    // $ANTLR start "rule__Encabezado__Group__1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:739:1: rule__Encabezado__Group__1 : rule__Encabezado__Group__1__Impl rule__Encabezado__Group__2 ;
    public final void rule__Encabezado__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:743:1: ( rule__Encabezado__Group__1__Impl rule__Encabezado__Group__2 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:744:2: rule__Encabezado__Group__1__Impl rule__Encabezado__Group__2
            {
            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__Group__1__Impl_in_rule__Encabezado__Group__11439);
            rule__Encabezado__Group__1__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__Group__2_in_rule__Encabezado__Group__11442);
            rule__Encabezado__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__Group__1"


    // $ANTLR start "rule__Encabezado__Group__1__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:751:1: rule__Encabezado__Group__1__Impl : ( ( rule__Encabezado__ColumnasAssignment_1 ) ) ;
    public final void rule__Encabezado__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:755:1: ( ( ( rule__Encabezado__ColumnasAssignment_1 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:756:1: ( ( rule__Encabezado__ColumnasAssignment_1 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:756:1: ( ( rule__Encabezado__ColumnasAssignment_1 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:757:1: ( rule__Encabezado__ColumnasAssignment_1 )
            {
             before(grammarAccess.getEncabezadoAccess().getColumnasAssignment_1()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:758:1: ( rule__Encabezado__ColumnasAssignment_1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:758:2: rule__Encabezado__ColumnasAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__ColumnasAssignment_1_in_rule__Encabezado__Group__1__Impl1469);
            rule__Encabezado__ColumnasAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEncabezadoAccess().getColumnasAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__Group__1__Impl"


    // $ANTLR start "rule__Encabezado__Group__2"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:768:1: rule__Encabezado__Group__2 : rule__Encabezado__Group__2__Impl ;
    public final void rule__Encabezado__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:772:1: ( rule__Encabezado__Group__2__Impl )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:773:2: rule__Encabezado__Group__2__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__Group__2__Impl_in_rule__Encabezado__Group__21499);
            rule__Encabezado__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__Group__2"


    // $ANTLR start "rule__Encabezado__Group__2__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:779:1: rule__Encabezado__Group__2__Impl : ( ( rule__Encabezado__Group_2__0 )* ) ;
    public final void rule__Encabezado__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:783:1: ( ( ( rule__Encabezado__Group_2__0 )* ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:784:1: ( ( rule__Encabezado__Group_2__0 )* )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:784:1: ( ( rule__Encabezado__Group_2__0 )* )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:785:1: ( rule__Encabezado__Group_2__0 )*
            {
             before(grammarAccess.getEncabezadoAccess().getGroup_2()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:786:1: ( rule__Encabezado__Group_2__0 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_SEPARATOR) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:786:2: rule__Encabezado__Group_2__0
            	    {
            	    pushFollow(FollowSets000.FOLLOW_rule__Encabezado__Group_2__0_in_rule__Encabezado__Group__2__Impl1526);
            	    rule__Encabezado__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getEncabezadoAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__Group__2__Impl"


    // $ANTLR start "rule__Encabezado__Group_2__0"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:802:1: rule__Encabezado__Group_2__0 : rule__Encabezado__Group_2__0__Impl rule__Encabezado__Group_2__1 ;
    public final void rule__Encabezado__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:806:1: ( rule__Encabezado__Group_2__0__Impl rule__Encabezado__Group_2__1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:807:2: rule__Encabezado__Group_2__0__Impl rule__Encabezado__Group_2__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__Group_2__0__Impl_in_rule__Encabezado__Group_2__01563);
            rule__Encabezado__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__Group_2__1_in_rule__Encabezado__Group_2__01566);
            rule__Encabezado__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__Group_2__0"


    // $ANTLR start "rule__Encabezado__Group_2__0__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:814:1: rule__Encabezado__Group_2__0__Impl : ( RULE_SEPARATOR ) ;
    public final void rule__Encabezado__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:818:1: ( ( RULE_SEPARATOR ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:819:1: ( RULE_SEPARATOR )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:819:1: ( RULE_SEPARATOR )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:820:1: RULE_SEPARATOR
            {
             before(grammarAccess.getEncabezadoAccess().getSEPARATORTerminalRuleCall_2_0()); 
            match(input,RULE_SEPARATOR,FollowSets000.FOLLOW_RULE_SEPARATOR_in_rule__Encabezado__Group_2__0__Impl1593); 
             after(grammarAccess.getEncabezadoAccess().getSEPARATORTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__Group_2__0__Impl"


    // $ANTLR start "rule__Encabezado__Group_2__1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:831:1: rule__Encabezado__Group_2__1 : rule__Encabezado__Group_2__1__Impl ;
    public final void rule__Encabezado__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:835:1: ( rule__Encabezado__Group_2__1__Impl )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:836:2: rule__Encabezado__Group_2__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__Group_2__1__Impl_in_rule__Encabezado__Group_2__11622);
            rule__Encabezado__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__Group_2__1"


    // $ANTLR start "rule__Encabezado__Group_2__1__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:842:1: rule__Encabezado__Group_2__1__Impl : ( ( rule__Encabezado__ColumnasAssignment_2_1 ) ) ;
    public final void rule__Encabezado__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:846:1: ( ( ( rule__Encabezado__ColumnasAssignment_2_1 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:847:1: ( ( rule__Encabezado__ColumnasAssignment_2_1 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:847:1: ( ( rule__Encabezado__ColumnasAssignment_2_1 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:848:1: ( rule__Encabezado__ColumnasAssignment_2_1 )
            {
             before(grammarAccess.getEncabezadoAccess().getColumnasAssignment_2_1()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:849:1: ( rule__Encabezado__ColumnasAssignment_2_1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:849:2: rule__Encabezado__ColumnasAssignment_2_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Encabezado__ColumnasAssignment_2_1_in_rule__Encabezado__Group_2__1__Impl1649);
            rule__Encabezado__ColumnasAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getEncabezadoAccess().getColumnasAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__Group_2__1__Impl"


    // $ANTLR start "rule__Elemento__Group__0"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:863:1: rule__Elemento__Group__0 : rule__Elemento__Group__0__Impl rule__Elemento__Group__1 ;
    public final void rule__Elemento__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:867:1: ( rule__Elemento__Group__0__Impl rule__Elemento__Group__1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:868:2: rule__Elemento__Group__0__Impl rule__Elemento__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Elemento__Group__0__Impl_in_rule__Elemento__Group__01683);
            rule__Elemento__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__Elemento__Group__1_in_rule__Elemento__Group__01686);
            rule__Elemento__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Elemento__Group__0"


    // $ANTLR start "rule__Elemento__Group__0__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:875:1: rule__Elemento__Group__0__Impl : ( () ) ;
    public final void rule__Elemento__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:879:1: ( ( () ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:880:1: ( () )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:880:1: ( () )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:881:1: ()
            {
             before(grammarAccess.getElementoAccess().getElementoAction_0()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:882:1: ()
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:884:1: 
            {
            }

             after(grammarAccess.getElementoAccess().getElementoAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Elemento__Group__0__Impl"


    // $ANTLR start "rule__Elemento__Group__1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:894:1: rule__Elemento__Group__1 : rule__Elemento__Group__1__Impl ;
    public final void rule__Elemento__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:898:1: ( rule__Elemento__Group__1__Impl )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:899:2: rule__Elemento__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__Elemento__Group__1__Impl_in_rule__Elemento__Group__11744);
            rule__Elemento__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Elemento__Group__1"


    // $ANTLR start "rule__Elemento__Group__1__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:905:1: rule__Elemento__Group__1__Impl : ( ( rule__Elemento__ValorElementoAssignment_1 ) ) ;
    public final void rule__Elemento__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:909:1: ( ( ( rule__Elemento__ValorElementoAssignment_1 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:910:1: ( ( rule__Elemento__ValorElementoAssignment_1 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:910:1: ( ( rule__Elemento__ValorElementoAssignment_1 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:911:1: ( rule__Elemento__ValorElementoAssignment_1 )
            {
             before(grammarAccess.getElementoAccess().getValorElementoAssignment_1()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:912:1: ( rule__Elemento__ValorElementoAssignment_1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:912:2: rule__Elemento__ValorElementoAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__Elemento__ValorElementoAssignment_1_in_rule__Elemento__Group__1__Impl1771);
            rule__Elemento__ValorElementoAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getElementoAccess().getValorElementoAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Elemento__Group__1__Impl"


    // $ANTLR start "rule__DescripcionColumna__Group__0"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:926:1: rule__DescripcionColumna__Group__0 : rule__DescripcionColumna__Group__0__Impl rule__DescripcionColumna__Group__1 ;
    public final void rule__DescripcionColumna__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:930:1: ( rule__DescripcionColumna__Group__0__Impl rule__DescripcionColumna__Group__1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:931:2: rule__DescripcionColumna__Group__0__Impl rule__DescripcionColumna__Group__1
            {
            pushFollow(FollowSets000.FOLLOW_rule__DescripcionColumna__Group__0__Impl_in_rule__DescripcionColumna__Group__01805);
            rule__DescripcionColumna__Group__0__Impl();

            state._fsp--;

            pushFollow(FollowSets000.FOLLOW_rule__DescripcionColumna__Group__1_in_rule__DescripcionColumna__Group__01808);
            rule__DescripcionColumna__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DescripcionColumna__Group__0"


    // $ANTLR start "rule__DescripcionColumna__Group__0__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:938:1: rule__DescripcionColumna__Group__0__Impl : ( () ) ;
    public final void rule__DescripcionColumna__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:942:1: ( ( () ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:943:1: ( () )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:943:1: ( () )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:944:1: ()
            {
             before(grammarAccess.getDescripcionColumnaAccess().getDescripcionColumnaAction_0()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:945:1: ()
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:947:1: 
            {
            }

             after(grammarAccess.getDescripcionColumnaAccess().getDescripcionColumnaAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DescripcionColumna__Group__0__Impl"


    // $ANTLR start "rule__DescripcionColumna__Group__1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:957:1: rule__DescripcionColumna__Group__1 : rule__DescripcionColumna__Group__1__Impl ;
    public final void rule__DescripcionColumna__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:961:1: ( rule__DescripcionColumna__Group__1__Impl )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:962:2: rule__DescripcionColumna__Group__1__Impl
            {
            pushFollow(FollowSets000.FOLLOW_rule__DescripcionColumna__Group__1__Impl_in_rule__DescripcionColumna__Group__11866);
            rule__DescripcionColumna__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DescripcionColumna__Group__1"


    // $ANTLR start "rule__DescripcionColumna__Group__1__Impl"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:968:1: rule__DescripcionColumna__Group__1__Impl : ( ( rule__DescripcionColumna__DescripcionAssignment_1 ) ) ;
    public final void rule__DescripcionColumna__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:972:1: ( ( ( rule__DescripcionColumna__DescripcionAssignment_1 ) ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:973:1: ( ( rule__DescripcionColumna__DescripcionAssignment_1 ) )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:973:1: ( ( rule__DescripcionColumna__DescripcionAssignment_1 ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:974:1: ( rule__DescripcionColumna__DescripcionAssignment_1 )
            {
             before(grammarAccess.getDescripcionColumnaAccess().getDescripcionAssignment_1()); 
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:975:1: ( rule__DescripcionColumna__DescripcionAssignment_1 )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:975:2: rule__DescripcionColumna__DescripcionAssignment_1
            {
            pushFollow(FollowSets000.FOLLOW_rule__DescripcionColumna__DescripcionAssignment_1_in_rule__DescripcionColumna__Group__1__Impl1893);
            rule__DescripcionColumna__DescripcionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDescripcionColumnaAccess().getDescripcionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DescripcionColumna__Group__1__Impl"


    // $ANTLR start "rule__Reporte__TipoReporteAssignment_0"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:990:1: rule__Reporte__TipoReporteAssignment_0 : ( ruleTipoReporte ) ;
    public final void rule__Reporte__TipoReporteAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:994:1: ( ( ruleTipoReporte ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:995:1: ( ruleTipoReporte )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:995:1: ( ruleTipoReporte )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:996:1: ruleTipoReporte
            {
             before(grammarAccess.getReporteAccess().getTipoReporteTipoReporteEnumRuleCall_0_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleTipoReporte_in_rule__Reporte__TipoReporteAssignment_01932);
            ruleTipoReporte();

            state._fsp--;

             after(grammarAccess.getReporteAccess().getTipoReporteTipoReporteEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__TipoReporteAssignment_0"


    // $ANTLR start "rule__Reporte__ArquitecturaAssignment_2"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1005:1: rule__Reporte__ArquitecturaAssignment_2 : ( RULE_VALELEM ) ;
    public final void rule__Reporte__ArquitecturaAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1009:1: ( ( RULE_VALELEM ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1010:1: ( RULE_VALELEM )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1010:1: ( RULE_VALELEM )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1011:1: RULE_VALELEM
            {
             before(grammarAccess.getReporteAccess().getArquitecturaVALELEMTerminalRuleCall_2_0()); 
            match(input,RULE_VALELEM,FollowSets000.FOLLOW_RULE_VALELEM_in_rule__Reporte__ArquitecturaAssignment_21963); 
             after(grammarAccess.getReporteAccess().getArquitecturaVALELEMTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__ArquitecturaAssignment_2"


    // $ANTLR start "rule__Reporte__EncabezadoAssignment_4"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1020:1: rule__Reporte__EncabezadoAssignment_4 : ( ruleEncabezado ) ;
    public final void rule__Reporte__EncabezadoAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1024:1: ( ( ruleEncabezado ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1025:1: ( ruleEncabezado )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1025:1: ( ruleEncabezado )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1026:1: ruleEncabezado
            {
             before(grammarAccess.getReporteAccess().getEncabezadoEncabezadoParserRuleCall_4_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleEncabezado_in_rule__Reporte__EncabezadoAssignment_41994);
            ruleEncabezado();

            state._fsp--;

             after(grammarAccess.getReporteAccess().getEncabezadoEncabezadoParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__EncabezadoAssignment_4"


    // $ANTLR start "rule__Reporte__RegistrosAssignment_6"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1035:1: rule__Reporte__RegistrosAssignment_6 : ( ruleRegistro ) ;
    public final void rule__Reporte__RegistrosAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1039:1: ( ( ruleRegistro ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1040:1: ( ruleRegistro )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1040:1: ( ruleRegistro )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1041:1: ruleRegistro
            {
             before(grammarAccess.getReporteAccess().getRegistrosRegistroParserRuleCall_6_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleRegistro_in_rule__Reporte__RegistrosAssignment_62025);
            ruleRegistro();

            state._fsp--;

             after(grammarAccess.getReporteAccess().getRegistrosRegistroParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__RegistrosAssignment_6"


    // $ANTLR start "rule__Reporte__RegistrosAssignment_7_1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1050:1: rule__Reporte__RegistrosAssignment_7_1 : ( ruleRegistro ) ;
    public final void rule__Reporte__RegistrosAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1054:1: ( ( ruleRegistro ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1055:1: ( ruleRegistro )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1055:1: ( ruleRegistro )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1056:1: ruleRegistro
            {
             before(grammarAccess.getReporteAccess().getRegistrosRegistroParserRuleCall_7_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleRegistro_in_rule__Reporte__RegistrosAssignment_7_12056);
            ruleRegistro();

            state._fsp--;

             after(grammarAccess.getReporteAccess().getRegistrosRegistroParserRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Reporte__RegistrosAssignment_7_1"


    // $ANTLR start "rule__Registro__ElementosAssignment_1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1065:1: rule__Registro__ElementosAssignment_1 : ( ruleElemento ) ;
    public final void rule__Registro__ElementosAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1069:1: ( ( ruleElemento ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1070:1: ( ruleElemento )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1070:1: ( ruleElemento )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1071:1: ruleElemento
            {
             before(grammarAccess.getRegistroAccess().getElementosElementoParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleElemento_in_rule__Registro__ElementosAssignment_12087);
            ruleElemento();

            state._fsp--;

             after(grammarAccess.getRegistroAccess().getElementosElementoParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__ElementosAssignment_1"


    // $ANTLR start "rule__Registro__ElementosAssignment_2_1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1080:1: rule__Registro__ElementosAssignment_2_1 : ( ruleElemento ) ;
    public final void rule__Registro__ElementosAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1084:1: ( ( ruleElemento ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1085:1: ( ruleElemento )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1085:1: ( ruleElemento )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1086:1: ruleElemento
            {
             before(grammarAccess.getRegistroAccess().getElementosElementoParserRuleCall_2_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleElemento_in_rule__Registro__ElementosAssignment_2_12118);
            ruleElemento();

            state._fsp--;

             after(grammarAccess.getRegistroAccess().getElementosElementoParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Registro__ElementosAssignment_2_1"


    // $ANTLR start "rule__Encabezado__ColumnasAssignment_1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1095:1: rule__Encabezado__ColumnasAssignment_1 : ( ruleDescripcionColumna ) ;
    public final void rule__Encabezado__ColumnasAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1099:1: ( ( ruleDescripcionColumna ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1100:1: ( ruleDescripcionColumna )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1100:1: ( ruleDescripcionColumna )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1101:1: ruleDescripcionColumna
            {
             before(grammarAccess.getEncabezadoAccess().getColumnasDescripcionColumnaParserRuleCall_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleDescripcionColumna_in_rule__Encabezado__ColumnasAssignment_12149);
            ruleDescripcionColumna();

            state._fsp--;

             after(grammarAccess.getEncabezadoAccess().getColumnasDescripcionColumnaParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__ColumnasAssignment_1"


    // $ANTLR start "rule__Encabezado__ColumnasAssignment_2_1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1110:1: rule__Encabezado__ColumnasAssignment_2_1 : ( ruleDescripcionColumna ) ;
    public final void rule__Encabezado__ColumnasAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1114:1: ( ( ruleDescripcionColumna ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1115:1: ( ruleDescripcionColumna )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1115:1: ( ruleDescripcionColumna )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1116:1: ruleDescripcionColumna
            {
             before(grammarAccess.getEncabezadoAccess().getColumnasDescripcionColumnaParserRuleCall_2_1_0()); 
            pushFollow(FollowSets000.FOLLOW_ruleDescripcionColumna_in_rule__Encabezado__ColumnasAssignment_2_12180);
            ruleDescripcionColumna();

            state._fsp--;

             after(grammarAccess.getEncabezadoAccess().getColumnasDescripcionColumnaParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Encabezado__ColumnasAssignment_2_1"


    // $ANTLR start "rule__Elemento__ValorElementoAssignment_1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1125:1: rule__Elemento__ValorElementoAssignment_1 : ( RULE_VALELEM ) ;
    public final void rule__Elemento__ValorElementoAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1129:1: ( ( RULE_VALELEM ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1130:1: ( RULE_VALELEM )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1130:1: ( RULE_VALELEM )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1131:1: RULE_VALELEM
            {
             before(grammarAccess.getElementoAccess().getValorElementoVALELEMTerminalRuleCall_1_0()); 
            match(input,RULE_VALELEM,FollowSets000.FOLLOW_RULE_VALELEM_in_rule__Elemento__ValorElementoAssignment_12211); 
             after(grammarAccess.getElementoAccess().getValorElementoVALELEMTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Elemento__ValorElementoAssignment_1"


    // $ANTLR start "rule__DescripcionColumna__DescripcionAssignment_1"
    // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1140:1: rule__DescripcionColumna__DescripcionAssignment_1 : ( RULE_VALELEM ) ;
    public final void rule__DescripcionColumna__DescripcionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1144:1: ( ( RULE_VALELEM ) )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1145:1: ( RULE_VALELEM )
            {
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1145:1: ( RULE_VALELEM )
            // ../uniandes.mdd.xtext.jmeterlogdsl.ui/src-gen/uniandes/mdd/jmeterlogdsl/ui/contentassist/antlr/internal/InternalJmeterLogDsl.g:1146:1: RULE_VALELEM
            {
             before(grammarAccess.getDescripcionColumnaAccess().getDescripcionVALELEMTerminalRuleCall_1_0()); 
            match(input,RULE_VALELEM,FollowSets000.FOLLOW_RULE_VALELEM_in_rule__DescripcionColumna__DescripcionAssignment_12242); 
             after(grammarAccess.getDescripcionColumnaAccess().getDescripcionVALELEMTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DescripcionColumna__DescripcionAssignment_1"

    // Delegated rules


 

    
    private static class FollowSets000 {
        public static final BitSet FOLLOW_ruleReporte_in_entryRuleReporte61 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleReporte68 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group__0_in_ruleReporte94 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleRegistro_in_entryRuleRegistro121 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleRegistro128 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Registro__Group__0_in_ruleRegistro154 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEncabezado_in_entryRuleEncabezado181 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleEncabezado188 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Encabezado__Group__0_in_ruleEncabezado214 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleElemento_in_entryRuleElemento241 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleElemento248 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Elemento__Group__0_in_ruleElemento274 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDescripcionColumna_in_entryRuleDescripcionColumna301 = new BitSet(new long[]{0x0000000000000000L});
        public static final BitSet FOLLOW_EOF_in_entryRuleDescripcionColumna308 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DescripcionColumna__Group__0_in_ruleDescripcionColumna334 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__TipoReporte__Alternatives_in_ruleTipoReporte371 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_7_in_rule__TipoReporte__Alternatives407 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_8_in_rule__TipoReporte__Alternatives428 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group__0__Impl_in_rule__Reporte__Group__0461 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Reporte__Group__1_in_rule__Reporte__Group__0464 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__TipoReporteAssignment_0_in_rule__Reporte__Group__0__Impl491 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group__1__Impl_in_rule__Reporte__Group__1521 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__Reporte__Group__2_in_rule__Reporte__Group__1524 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SEPARATOR_in_rule__Reporte__Group__1__Impl551 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group__2__Impl_in_rule__Reporte__Group__2580 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__Reporte__Group__3_in_rule__Reporte__Group__2583 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__ArquitecturaAssignment_2_in_rule__Reporte__Group__2__Impl610 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group__3__Impl_in_rule__Reporte__Group__3640 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__Reporte__Group__4_in_rule__Reporte__Group__3643 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_NEWLINE_in_rule__Reporte__Group__3__Impl670 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group__4__Impl_in_rule__Reporte__Group__4699 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__Reporte__Group__5_in_rule__Reporte__Group__4702 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__EncabezadoAssignment_4_in_rule__Reporte__Group__4__Impl729 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group__5__Impl_in_rule__Reporte__Group__5759 = new BitSet(new long[]{0x0000000000000050L});
        public static final BitSet FOLLOW_rule__Reporte__Group__6_in_rule__Reporte__Group__5762 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_NEWLINE_in_rule__Reporte__Group__5__Impl789 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group__6__Impl_in_rule__Reporte__Group__6818 = new BitSet(new long[]{0x0000000000000020L});
        public static final BitSet FOLLOW_rule__Reporte__Group__7_in_rule__Reporte__Group__6821 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__RegistrosAssignment_6_in_rule__Reporte__Group__6__Impl848 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group__7__Impl_in_rule__Reporte__Group__7878 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group_7__0_in_rule__Reporte__Group__7__Impl905 = new BitSet(new long[]{0x0000000000000022L});
        public static final BitSet FOLLOW_rule__Reporte__Group_7__0__Impl_in_rule__Reporte__Group_7__0952 = new BitSet(new long[]{0x0000000000000050L});
        public static final BitSet FOLLOW_rule__Reporte__Group_7__1_in_rule__Reporte__Group_7__0955 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_NEWLINE_in_rule__Reporte__Group_7__0__Impl982 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__Group_7__1__Impl_in_rule__Reporte__Group_7__11011 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Reporte__RegistrosAssignment_7_1_in_rule__Reporte__Group_7__1__Impl1038 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Registro__Group__0__Impl_in_rule__Registro__Group__01072 = new BitSet(new long[]{0x0000000000000050L});
        public static final BitSet FOLLOW_rule__Registro__Group__1_in_rule__Registro__Group__01075 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Registro__Group__1__Impl_in_rule__Registro__Group__11133 = new BitSet(new long[]{0x0000000000000050L});
        public static final BitSet FOLLOW_rule__Registro__Group__2_in_rule__Registro__Group__11136 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Registro__ElementosAssignment_1_in_rule__Registro__Group__1__Impl1163 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Registro__Group__2__Impl_in_rule__Registro__Group__21194 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Registro__Group_2__0_in_rule__Registro__Group__2__Impl1221 = new BitSet(new long[]{0x0000000000000012L});
        public static final BitSet FOLLOW_rule__Registro__Group_2__0__Impl_in_rule__Registro__Group_2__01258 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__Registro__Group_2__1_in_rule__Registro__Group_2__01261 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SEPARATOR_in_rule__Registro__Group_2__0__Impl1288 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Registro__Group_2__1__Impl_in_rule__Registro__Group_2__11317 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Registro__ElementosAssignment_2_1_in_rule__Registro__Group_2__1__Impl1344 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Encabezado__Group__0__Impl_in_rule__Encabezado__Group__01378 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__Encabezado__Group__1_in_rule__Encabezado__Group__01381 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Encabezado__Group__1__Impl_in_rule__Encabezado__Group__11439 = new BitSet(new long[]{0x0000000000000010L});
        public static final BitSet FOLLOW_rule__Encabezado__Group__2_in_rule__Encabezado__Group__11442 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Encabezado__ColumnasAssignment_1_in_rule__Encabezado__Group__1__Impl1469 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Encabezado__Group__2__Impl_in_rule__Encabezado__Group__21499 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Encabezado__Group_2__0_in_rule__Encabezado__Group__2__Impl1526 = new BitSet(new long[]{0x0000000000000012L});
        public static final BitSet FOLLOW_rule__Encabezado__Group_2__0__Impl_in_rule__Encabezado__Group_2__01563 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__Encabezado__Group_2__1_in_rule__Encabezado__Group_2__01566 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_SEPARATOR_in_rule__Encabezado__Group_2__0__Impl1593 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Encabezado__Group_2__1__Impl_in_rule__Encabezado__Group_2__11622 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Encabezado__ColumnasAssignment_2_1_in_rule__Encabezado__Group_2__1__Impl1649 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Elemento__Group__0__Impl_in_rule__Elemento__Group__01683 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__Elemento__Group__1_in_rule__Elemento__Group__01686 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Elemento__Group__1__Impl_in_rule__Elemento__Group__11744 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__Elemento__ValorElementoAssignment_1_in_rule__Elemento__Group__1__Impl1771 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DescripcionColumna__Group__0__Impl_in_rule__DescripcionColumna__Group__01805 = new BitSet(new long[]{0x0000000000000040L});
        public static final BitSet FOLLOW_rule__DescripcionColumna__Group__1_in_rule__DescripcionColumna__Group__01808 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DescripcionColumna__Group__1__Impl_in_rule__DescripcionColumna__Group__11866 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_rule__DescripcionColumna__DescripcionAssignment_1_in_rule__DescripcionColumna__Group__1__Impl1893 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleTipoReporte_in_rule__Reporte__TipoReporteAssignment_01932 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_VALELEM_in_rule__Reporte__ArquitecturaAssignment_21963 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleEncabezado_in_rule__Reporte__EncabezadoAssignment_41994 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleRegistro_in_rule__Reporte__RegistrosAssignment_62025 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleRegistro_in_rule__Reporte__RegistrosAssignment_7_12056 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleElemento_in_rule__Registro__ElementosAssignment_12087 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleElemento_in_rule__Registro__ElementosAssignment_2_12118 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDescripcionColumna_in_rule__Encabezado__ColumnasAssignment_12149 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_ruleDescripcionColumna_in_rule__Encabezado__ColumnasAssignment_2_12180 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_VALELEM_in_rule__Elemento__ValorElementoAssignment_12211 = new BitSet(new long[]{0x0000000000000002L});
        public static final BitSet FOLLOW_RULE_VALELEM_in_rule__DescripcionColumna__DescripcionAssignment_12242 = new BitSet(new long[]{0x0000000000000002L});
    }


}