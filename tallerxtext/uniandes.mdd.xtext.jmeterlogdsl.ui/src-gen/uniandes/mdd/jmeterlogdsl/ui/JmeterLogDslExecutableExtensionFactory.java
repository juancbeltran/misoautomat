/*
 * generated by Xtext
 */
package uniandes.mdd.jmeterlogdsl.ui;

import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import uniandes.mdd.jmeterlogdsl.ui.internal.JmeterLogDslActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class JmeterLogDslExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return JmeterLogDslActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return JmeterLogDslActivator.getInstance().getInjector(JmeterLogDslActivator.UNIANDES_MDD_JMETERLOGDSL_JMETERLOGDSL);
	}
	
}
