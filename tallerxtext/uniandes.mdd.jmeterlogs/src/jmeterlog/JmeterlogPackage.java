/**
 */
package jmeterlog;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see jmeterlog.JmeterlogFactory
 * @model kind="package"
 * @generated
 */
public interface JmeterlogPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "jmeterlog";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.uniandes.edu.co/jmeterlog";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "jmeterlog";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JmeterlogPackage eINSTANCE = jmeterlog.impl.JmeterlogPackageImpl.init();

	/**
	 * The meta object id for the '{@link jmeterlog.impl.ReporteImpl <em>Reporte</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jmeterlog.impl.ReporteImpl
	 * @see jmeterlog.impl.JmeterlogPackageImpl#getReporte()
	 * @generated
	 */
	int REPORTE = 0;

	/**
	 * The feature id for the '<em><b>Tipo Reporte</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORTE__TIPO_REPORTE = 0;

	/**
	 * The feature id for the '<em><b>Arquitectura</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORTE__ARQUITECTURA = 1;

	/**
	 * The feature id for the '<em><b>Registros</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORTE__REGISTROS = 2;

	/**
	 * The feature id for the '<em><b>Encabezado</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORTE__ENCABEZADO = 3;

	/**
	 * The number of structural features of the '<em>Reporte</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORTE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Reporte</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPORTE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link jmeterlog.impl.RegistroImpl <em>Registro</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jmeterlog.impl.RegistroImpl
	 * @see jmeterlog.impl.JmeterlogPackageImpl#getRegistro()
	 * @generated
	 */
	int REGISTRO = 1;

	/**
	 * The feature id for the '<em><b>Elementos</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTRO__ELEMENTOS = 0;

	/**
	 * The number of structural features of the '<em>Registro</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTRO_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Registro</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGISTRO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link jmeterlog.impl.ElementoImpl <em>Elemento</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jmeterlog.impl.ElementoImpl
	 * @see jmeterlog.impl.JmeterlogPackageImpl#getElemento()
	 * @generated
	 */
	int ELEMENTO = 2;

	/**
	 * The feature id for the '<em><b>Valor Elemento</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENTO__VALOR_ELEMENTO = 0;

	/**
	 * The number of structural features of the '<em>Elemento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENTO_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Elemento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link jmeterlog.impl.DescripcionColumnaImpl <em>Descripcion Columna</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jmeterlog.impl.DescripcionColumnaImpl
	 * @see jmeterlog.impl.JmeterlogPackageImpl#getDescripcionColumna()
	 * @generated
	 */
	int DESCRIPCION_COLUMNA = 3;

	/**
	 * The feature id for the '<em><b>Descripcion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPCION_COLUMNA__DESCRIPCION = 0;

	/**
	 * The number of structural features of the '<em>Descripcion Columna</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPCION_COLUMNA_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Descripcion Columna</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESCRIPCION_COLUMNA_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link jmeterlog.impl.EncabezadoImpl <em>Encabezado</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jmeterlog.impl.EncabezadoImpl
	 * @see jmeterlog.impl.JmeterlogPackageImpl#getEncabezado()
	 * @generated
	 */
	int ENCABEZADO = 4;

	/**
	 * The feature id for the '<em><b>Columnas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCABEZADO__COLUMNAS = 0;

	/**
	 * The number of structural features of the '<em>Encabezado</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCABEZADO_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Encabezado</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCABEZADO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link jmeterlog.TipoReporte <em>Tipo Reporte</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see jmeterlog.TipoReporte
	 * @see jmeterlog.impl.JmeterlogPackageImpl#getTipoReporte()
	 * @generated
	 */
	int TIPO_REPORTE = 5;


	/**
	 * Returns the meta object for class '{@link jmeterlog.Reporte <em>Reporte</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reporte</em>'.
	 * @see jmeterlog.Reporte
	 * @generated
	 */
	EClass getReporte();

	/**
	 * Returns the meta object for the attribute '{@link jmeterlog.Reporte#getTipoReporte <em>Tipo Reporte</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tipo Reporte</em>'.
	 * @see jmeterlog.Reporte#getTipoReporte()
	 * @see #getReporte()
	 * @generated
	 */
	EAttribute getReporte_TipoReporte();

	/**
	 * Returns the meta object for the attribute '{@link jmeterlog.Reporte#getArquitectura <em>Arquitectura</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Arquitectura</em>'.
	 * @see jmeterlog.Reporte#getArquitectura()
	 * @see #getReporte()
	 * @generated
	 */
	EAttribute getReporte_Arquitectura();

	/**
	 * Returns the meta object for the containment reference list '{@link jmeterlog.Reporte#getRegistros <em>Registros</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Registros</em>'.
	 * @see jmeterlog.Reporte#getRegistros()
	 * @see #getReporte()
	 * @generated
	 */
	EReference getReporte_Registros();

	/**
	 * Returns the meta object for the containment reference '{@link jmeterlog.Reporte#getEncabezado <em>Encabezado</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Encabezado</em>'.
	 * @see jmeterlog.Reporte#getEncabezado()
	 * @see #getReporte()
	 * @generated
	 */
	EReference getReporte_Encabezado();

	/**
	 * Returns the meta object for class '{@link jmeterlog.Registro <em>Registro</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Registro</em>'.
	 * @see jmeterlog.Registro
	 * @generated
	 */
	EClass getRegistro();

	/**
	 * Returns the meta object for the containment reference list '{@link jmeterlog.Registro#getElementos <em>Elementos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elementos</em>'.
	 * @see jmeterlog.Registro#getElementos()
	 * @see #getRegistro()
	 * @generated
	 */
	EReference getRegistro_Elementos();

	/**
	 * Returns the meta object for class '{@link jmeterlog.Elemento <em>Elemento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Elemento</em>'.
	 * @see jmeterlog.Elemento
	 * @generated
	 */
	EClass getElemento();

	/**
	 * Returns the meta object for the attribute '{@link jmeterlog.Elemento#getValorElemento <em>Valor Elemento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Valor Elemento</em>'.
	 * @see jmeterlog.Elemento#getValorElemento()
	 * @see #getElemento()
	 * @generated
	 */
	EAttribute getElemento_ValorElemento();

	/**
	 * Returns the meta object for class '{@link jmeterlog.DescripcionColumna <em>Descripcion Columna</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Descripcion Columna</em>'.
	 * @see jmeterlog.DescripcionColumna
	 * @generated
	 */
	EClass getDescripcionColumna();

	/**
	 * Returns the meta object for the attribute '{@link jmeterlog.DescripcionColumna#getDescripcion <em>Descripcion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Descripcion</em>'.
	 * @see jmeterlog.DescripcionColumna#getDescripcion()
	 * @see #getDescripcionColumna()
	 * @generated
	 */
	EAttribute getDescripcionColumna_Descripcion();

	/**
	 * Returns the meta object for class '{@link jmeterlog.Encabezado <em>Encabezado</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Encabezado</em>'.
	 * @see jmeterlog.Encabezado
	 * @generated
	 */
	EClass getEncabezado();

	/**
	 * Returns the meta object for the containment reference list '{@link jmeterlog.Encabezado#getColumnas <em>Columnas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Columnas</em>'.
	 * @see jmeterlog.Encabezado#getColumnas()
	 * @see #getEncabezado()
	 * @generated
	 */
	EReference getEncabezado_Columnas();

	/**
	 * Returns the meta object for enum '{@link jmeterlog.TipoReporte <em>Tipo Reporte</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Tipo Reporte</em>'.
	 * @see jmeterlog.TipoReporte
	 * @generated
	 */
	EEnum getTipoReporte();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	JmeterlogFactory getJmeterlogFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link jmeterlog.impl.ReporteImpl <em>Reporte</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jmeterlog.impl.ReporteImpl
		 * @see jmeterlog.impl.JmeterlogPackageImpl#getReporte()
		 * @generated
		 */
		EClass REPORTE = eINSTANCE.getReporte();

		/**
		 * The meta object literal for the '<em><b>Tipo Reporte</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORTE__TIPO_REPORTE = eINSTANCE.getReporte_TipoReporte();

		/**
		 * The meta object literal for the '<em><b>Arquitectura</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPORTE__ARQUITECTURA = eINSTANCE.getReporte_Arquitectura();

		/**
		 * The meta object literal for the '<em><b>Registros</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPORTE__REGISTROS = eINSTANCE.getReporte_Registros();

		/**
		 * The meta object literal for the '<em><b>Encabezado</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPORTE__ENCABEZADO = eINSTANCE.getReporte_Encabezado();

		/**
		 * The meta object literal for the '{@link jmeterlog.impl.RegistroImpl <em>Registro</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jmeterlog.impl.RegistroImpl
		 * @see jmeterlog.impl.JmeterlogPackageImpl#getRegistro()
		 * @generated
		 */
		EClass REGISTRO = eINSTANCE.getRegistro();

		/**
		 * The meta object literal for the '<em><b>Elementos</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGISTRO__ELEMENTOS = eINSTANCE.getRegistro_Elementos();

		/**
		 * The meta object literal for the '{@link jmeterlog.impl.ElementoImpl <em>Elemento</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jmeterlog.impl.ElementoImpl
		 * @see jmeterlog.impl.JmeterlogPackageImpl#getElemento()
		 * @generated
		 */
		EClass ELEMENTO = eINSTANCE.getElemento();

		/**
		 * The meta object literal for the '<em><b>Valor Elemento</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENTO__VALOR_ELEMENTO = eINSTANCE.getElemento_ValorElemento();

		/**
		 * The meta object literal for the '{@link jmeterlog.impl.DescripcionColumnaImpl <em>Descripcion Columna</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jmeterlog.impl.DescripcionColumnaImpl
		 * @see jmeterlog.impl.JmeterlogPackageImpl#getDescripcionColumna()
		 * @generated
		 */
		EClass DESCRIPCION_COLUMNA = eINSTANCE.getDescripcionColumna();

		/**
		 * The meta object literal for the '<em><b>Descripcion</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DESCRIPCION_COLUMNA__DESCRIPCION = eINSTANCE.getDescripcionColumna_Descripcion();

		/**
		 * The meta object literal for the '{@link jmeterlog.impl.EncabezadoImpl <em>Encabezado</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jmeterlog.impl.EncabezadoImpl
		 * @see jmeterlog.impl.JmeterlogPackageImpl#getEncabezado()
		 * @generated
		 */
		EClass ENCABEZADO = eINSTANCE.getEncabezado();

		/**
		 * The meta object literal for the '<em><b>Columnas</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENCABEZADO__COLUMNAS = eINSTANCE.getEncabezado_Columnas();

		/**
		 * The meta object literal for the '{@link jmeterlog.TipoReporte <em>Tipo Reporte</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see jmeterlog.TipoReporte
		 * @see jmeterlog.impl.JmeterlogPackageImpl#getTipoReporte()
		 * @generated
		 */
		EEnum TIPO_REPORTE = eINSTANCE.getTipoReporte();

	}

} //JmeterlogPackage
