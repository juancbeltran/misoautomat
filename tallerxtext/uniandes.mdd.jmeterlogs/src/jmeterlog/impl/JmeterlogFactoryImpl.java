/**
 */
package jmeterlog.impl;

import jmeterlog.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JmeterlogFactoryImpl extends EFactoryImpl implements JmeterlogFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static JmeterlogFactory init() {
		try {
			JmeterlogFactory theJmeterlogFactory = (JmeterlogFactory)EPackage.Registry.INSTANCE.getEFactory(JmeterlogPackage.eNS_URI);
			if (theJmeterlogFactory != null) {
				return theJmeterlogFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new JmeterlogFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JmeterlogFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case JmeterlogPackage.REPORTE: return createReporte();
			case JmeterlogPackage.REGISTRO: return createRegistro();
			case JmeterlogPackage.ELEMENTO: return createElemento();
			case JmeterlogPackage.DESCRIPCION_COLUMNA: return createDescripcionColumna();
			case JmeterlogPackage.ENCABEZADO: return createEncabezado();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case JmeterlogPackage.TIPO_REPORTE:
				return createTipoReporteFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case JmeterlogPackage.TIPO_REPORTE:
				return convertTipoReporteToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reporte createReporte() {
		ReporteImpl reporte = new ReporteImpl();
		return reporte;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Registro createRegistro() {
		RegistroImpl registro = new RegistroImpl();
		return registro;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Elemento createElemento() {
		ElementoImpl elemento = new ElementoImpl();
		return elemento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DescripcionColumna createDescripcionColumna() {
		DescripcionColumnaImpl descripcionColumna = new DescripcionColumnaImpl();
		return descripcionColumna;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Encabezado createEncabezado() {
		EncabezadoImpl encabezado = new EncabezadoImpl();
		return encabezado;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TipoReporte createTipoReporteFromString(EDataType eDataType, String initialValue) {
		TipoReporte result = TipoReporte.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTipoReporteToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JmeterlogPackage getJmeterlogPackage() {
		return (JmeterlogPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static JmeterlogPackage getPackage() {
		return JmeterlogPackage.eINSTANCE;
	}

} //JmeterlogFactoryImpl
