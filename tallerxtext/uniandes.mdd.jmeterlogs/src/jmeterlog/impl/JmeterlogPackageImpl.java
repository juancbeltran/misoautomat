/**
 */
package jmeterlog.impl;

import jmeterlog.DescripcionColumna;
import jmeterlog.Elemento;
import jmeterlog.Encabezado;
import jmeterlog.JmeterlogFactory;
import jmeterlog.JmeterlogPackage;
import jmeterlog.Registro;
import jmeterlog.Reporte;
import jmeterlog.TipoReporte;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JmeterlogPackageImpl extends EPackageImpl implements JmeterlogPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reporteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass registroEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elementoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass descripcionColumnaEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass encabezadoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum tipoReporteEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see jmeterlog.JmeterlogPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private JmeterlogPackageImpl() {
		super(eNS_URI, JmeterlogFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link JmeterlogPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static JmeterlogPackage init() {
		if (isInited) return (JmeterlogPackage)EPackage.Registry.INSTANCE.getEPackage(JmeterlogPackage.eNS_URI);

		// Obtain or create and register package
		JmeterlogPackageImpl theJmeterlogPackage = (JmeterlogPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof JmeterlogPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new JmeterlogPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theJmeterlogPackage.createPackageContents();

		// Initialize created meta-data
		theJmeterlogPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theJmeterlogPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(JmeterlogPackage.eNS_URI, theJmeterlogPackage);
		return theJmeterlogPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReporte() {
		return reporteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReporte_TipoReporte() {
		return (EAttribute)reporteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReporte_Arquitectura() {
		return (EAttribute)reporteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReporte_Registros() {
		return (EReference)reporteEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReporte_Encabezado() {
		return (EReference)reporteEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRegistro() {
		return registroEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegistro_Elementos() {
		return (EReference)registroEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElemento() {
		return elementoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getElemento_ValorElemento() {
		return (EAttribute)elementoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDescripcionColumna() {
		return descripcionColumnaEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDescripcionColumna_Descripcion() {
		return (EAttribute)descripcionColumnaEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEncabezado() {
		return encabezadoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEncabezado_Columnas() {
		return (EReference)encabezadoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTipoReporte() {
		return tipoReporteEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JmeterlogFactory getJmeterlogFactory() {
		return (JmeterlogFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		reporteEClass = createEClass(REPORTE);
		createEAttribute(reporteEClass, REPORTE__TIPO_REPORTE);
		createEAttribute(reporteEClass, REPORTE__ARQUITECTURA);
		createEReference(reporteEClass, REPORTE__REGISTROS);
		createEReference(reporteEClass, REPORTE__ENCABEZADO);

		registroEClass = createEClass(REGISTRO);
		createEReference(registroEClass, REGISTRO__ELEMENTOS);

		elementoEClass = createEClass(ELEMENTO);
		createEAttribute(elementoEClass, ELEMENTO__VALOR_ELEMENTO);

		descripcionColumnaEClass = createEClass(DESCRIPCION_COLUMNA);
		createEAttribute(descripcionColumnaEClass, DESCRIPCION_COLUMNA__DESCRIPCION);

		encabezadoEClass = createEClass(ENCABEZADO);
		createEReference(encabezadoEClass, ENCABEZADO__COLUMNAS);

		// Create enums
		tipoReporteEEnum = createEEnum(TIPO_REPORTE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(reporteEClass, Reporte.class, "Reporte", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReporte_TipoReporte(), this.getTipoReporte(), "TipoReporte", "Aggregate", 0, 1, Reporte.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getReporte_Arquitectura(), ecorePackage.getEString(), "Arquitectura", null, 0, 1, Reporte.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReporte_Registros(), this.getRegistro(), null, "Registros", null, 0, -1, Reporte.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getReporte_Encabezado(), this.getEncabezado(), null, "Encabezado", null, 1, 1, Reporte.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(registroEClass, Registro.class, "Registro", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRegistro_Elementos(), this.getElemento(), null, "Elementos", null, 0, -1, Registro.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(elementoEClass, Elemento.class, "Elemento", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getElemento_ValorElemento(), ecorePackage.getEString(), "ValorElemento", null, 0, 1, Elemento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(descripcionColumnaEClass, DescripcionColumna.class, "DescripcionColumna", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDescripcionColumna_Descripcion(), ecorePackage.getEString(), "Descripcion", null, 0, 1, DescripcionColumna.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(encabezadoEClass, Encabezado.class, "Encabezado", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEncabezado_Columnas(), this.getDescripcionColumna(), null, "Columnas", null, 0, -1, Encabezado.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(tipoReporteEEnum, TipoReporte.class, "TipoReporte");
		addEEnumLiteral(tipoReporteEEnum, TipoReporte.AGGREGATE);
		addEEnumLiteral(tipoReporteEEnum, TipoReporte.RESPONSE_LATENCY);

		// Create resource
		createResource(eNS_URI);
	}

} //JmeterlogPackageImpl
