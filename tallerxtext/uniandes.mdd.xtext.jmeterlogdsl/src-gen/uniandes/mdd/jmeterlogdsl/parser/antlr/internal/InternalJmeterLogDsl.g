/*
 * generated by Xtext
 */
grammar InternalJmeterLogDsl;

options {
	superClass=AbstractInternalAntlrParser;
	
}

@lexer::header {
package uniandes.mdd.jmeterlogdsl.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;
}

@parser::header {
package uniandes.mdd.jmeterlogdsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import uniandes.mdd.jmeterlogdsl.services.JmeterLogDslGrammarAccess;

}

@parser::members {

 	private JmeterLogDslGrammarAccess grammarAccess;
 	
    public InternalJmeterLogDslParser(TokenStream input, JmeterLogDslGrammarAccess grammarAccess) {
        this(input);
        this.grammarAccess = grammarAccess;
        registerRules(grammarAccess.getGrammar());
    }
    
    @Override
    protected String getFirstRuleName() {
    	return "Reporte";	
   	}
   	
   	@Override
   	protected JmeterLogDslGrammarAccess getGrammarAccess() {
   		return grammarAccess;
   	}
}

@rulecatch { 
    catch (RecognitionException re) { 
        recover(input,re); 
        appendSkippedTokens();
    } 
}




// Entry rule entryRuleReporte
entryRuleReporte returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getReporteRule()); }
	 iv_ruleReporte=ruleReporte 
	 { $current=$iv_ruleReporte.current; } 
	 EOF 
;

// Rule Reporte
ruleReporte returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
(
		{ 
	        newCompositeNode(grammarAccess.getReporteAccess().getTipoReporteTipoReporteEnumRuleCall_0_0()); 
	    }
		lv_TipoReporte_0_0=ruleTipoReporte		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getReporteRule());
	        }
       		set(
       			$current, 
       			"TipoReporte",
        		lv_TipoReporte_0_0, 
        		"TipoReporte");
	        afterParserOrEnumRuleCall();
	    }

)
)this_SEPARATOR_1=RULE_SEPARATOR
    { 
    newLeafNode(this_SEPARATOR_1, grammarAccess.getReporteAccess().getSEPARATORTerminalRuleCall_1()); 
    }
(
(
		lv_Arquitectura_2_0=RULE_VALELEM
		{
			newLeafNode(lv_Arquitectura_2_0, grammarAccess.getReporteAccess().getArquitecturaVALELEMTerminalRuleCall_2_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getReporteRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"Arquitectura",
        		lv_Arquitectura_2_0, 
        		"VALELEM");
	    }

)
)this_NEWLINE_3=RULE_NEWLINE
    { 
    newLeafNode(this_NEWLINE_3, grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_3()); 
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getReporteAccess().getEncabezadoEncabezadoParserRuleCall_4_0()); 
	    }
		lv_Encabezado_4_0=ruleEncabezado		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getReporteRule());
	        }
       		set(
       			$current, 
       			"Encabezado",
        		lv_Encabezado_4_0, 
        		"Encabezado");
	        afterParserOrEnumRuleCall();
	    }

)
)this_NEWLINE_5=RULE_NEWLINE
    { 
    newLeafNode(this_NEWLINE_5, grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_5()); 
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getReporteAccess().getRegistrosRegistroParserRuleCall_6_0()); 
	    }
		lv_Registros_6_0=ruleRegistro		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getReporteRule());
	        }
       		add(
       			$current, 
       			"Registros",
        		lv_Registros_6_0, 
        		"Registro");
	        afterParserOrEnumRuleCall();
	    }

)
)(this_NEWLINE_7=RULE_NEWLINE
    { 
    newLeafNode(this_NEWLINE_7, grammarAccess.getReporteAccess().getNEWLINETerminalRuleCall_7_0()); 
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getReporteAccess().getRegistrosRegistroParserRuleCall_7_1_0()); 
	    }
		lv_Registros_8_0=ruleRegistro		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getReporteRule());
	        }
       		add(
       			$current, 
       			"Registros",
        		lv_Registros_8_0, 
        		"Registro");
	        afterParserOrEnumRuleCall();
	    }

)
))*)
;





// Entry rule entryRuleRegistro
entryRuleRegistro returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getRegistroRule()); }
	 iv_ruleRegistro=ruleRegistro 
	 { $current=$iv_ruleRegistro.current; } 
	 EOF 
;

// Rule Registro
ruleRegistro returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getRegistroAccess().getRegistroAction_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getRegistroAccess().getElementosElementoParserRuleCall_1_0()); 
	    }
		lv_Elementos_1_0=ruleElemento		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getRegistroRule());
	        }
       		add(
       			$current, 
       			"Elementos",
        		lv_Elementos_1_0, 
        		"Elemento");
	        afterParserOrEnumRuleCall();
	    }

)
)?(this_SEPARATOR_2=RULE_SEPARATOR
    { 
    newLeafNode(this_SEPARATOR_2, grammarAccess.getRegistroAccess().getSEPARATORTerminalRuleCall_2_0()); 
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getRegistroAccess().getElementosElementoParserRuleCall_2_1_0()); 
	    }
		lv_Elementos_3_0=ruleElemento		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getRegistroRule());
	        }
       		add(
       			$current, 
       			"Elementos",
        		lv_Elementos_3_0, 
        		"Elemento");
	        afterParserOrEnumRuleCall();
	    }

)
))*)
;





// Entry rule entryRuleEncabezado
entryRuleEncabezado returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getEncabezadoRule()); }
	 iv_ruleEncabezado=ruleEncabezado 
	 { $current=$iv_ruleEncabezado.current; } 
	 EOF 
;

// Rule Encabezado
ruleEncabezado returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getEncabezadoAccess().getEncabezadoAction_0(),
            $current);
    }
)(
(
		{ 
	        newCompositeNode(grammarAccess.getEncabezadoAccess().getColumnasDescripcionColumnaParserRuleCall_1_0()); 
	    }
		lv_Columnas_1_0=ruleDescripcionColumna		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getEncabezadoRule());
	        }
       		add(
       			$current, 
       			"Columnas",
        		lv_Columnas_1_0, 
        		"DescripcionColumna");
	        afterParserOrEnumRuleCall();
	    }

)
)(this_SEPARATOR_2=RULE_SEPARATOR
    { 
    newLeafNode(this_SEPARATOR_2, grammarAccess.getEncabezadoAccess().getSEPARATORTerminalRuleCall_2_0()); 
    }
(
(
		{ 
	        newCompositeNode(grammarAccess.getEncabezadoAccess().getColumnasDescripcionColumnaParserRuleCall_2_1_0()); 
	    }
		lv_Columnas_3_0=ruleDescripcionColumna		{
	        if ($current==null) {
	            $current = createModelElementForParent(grammarAccess.getEncabezadoRule());
	        }
       		add(
       			$current, 
       			"Columnas",
        		lv_Columnas_3_0, 
        		"DescripcionColumna");
	        afterParserOrEnumRuleCall();
	    }

)
))*)
;





// Entry rule entryRuleElemento
entryRuleElemento returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getElementoRule()); }
	 iv_ruleElemento=ruleElemento 
	 { $current=$iv_ruleElemento.current; } 
	 EOF 
;

// Rule Elemento
ruleElemento returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getElementoAccess().getElementoAction_0(),
            $current);
    }
)(
(
		lv_ValorElemento_1_0=RULE_VALELEM
		{
			newLeafNode(lv_ValorElemento_1_0, grammarAccess.getElementoAccess().getValorElementoVALELEMTerminalRuleCall_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getElementoRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"ValorElemento",
        		lv_ValorElemento_1_0, 
        		"VALELEM");
	    }

)
))
;





// Entry rule entryRuleDescripcionColumna
entryRuleDescripcionColumna returns [EObject current=null] 
	:
	{ newCompositeNode(grammarAccess.getDescripcionColumnaRule()); }
	 iv_ruleDescripcionColumna=ruleDescripcionColumna 
	 { $current=$iv_ruleDescripcionColumna.current; } 
	 EOF 
;

// Rule DescripcionColumna
ruleDescripcionColumna returns [EObject current=null] 
    @init { enterRule(); 
    }
    @after { leaveRule(); }:
((
    {
        $current = forceCreateModelElement(
            grammarAccess.getDescripcionColumnaAccess().getDescripcionColumnaAction_0(),
            $current);
    }
)(
(
		lv_Descripcion_1_0=RULE_VALELEM
		{
			newLeafNode(lv_Descripcion_1_0, grammarAccess.getDescripcionColumnaAccess().getDescripcionVALELEMTerminalRuleCall_1_0()); 
		}
		{
	        if ($current==null) {
	            $current = createModelElement(grammarAccess.getDescripcionColumnaRule());
	        }
       		setWithLastConsumed(
       			$current, 
       			"Descripcion",
        		lv_Descripcion_1_0, 
        		"VALELEM");
	    }

)
))
;





// Rule TipoReporte
ruleTipoReporte returns [Enumerator current=null] 
    @init { enterRule(); }
    @after { leaveRule(); }:
((	enumLiteral_0='Aggregate' 
	{
        $current = grammarAccess.getTipoReporteAccess().getAggregateEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_0, grammarAccess.getTipoReporteAccess().getAggregateEnumLiteralDeclaration_0()); 
    }
)
    |(	enumLiteral_1='ResponseLatency' 
	{
        $current = grammarAccess.getTipoReporteAccess().getResponseLatencyEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
        newLeafNode(enumLiteral_1, grammarAccess.getTipoReporteAccess().getResponseLatencyEnumLiteralDeclaration_1()); 
    }
));



RULE_SEPARATOR : ',';

RULE_NEWLINE : '\r'? '\n';

RULE_VALELEM : ('"' .+ '"'|~(('"'|','|'\r'|'\n'))+);


